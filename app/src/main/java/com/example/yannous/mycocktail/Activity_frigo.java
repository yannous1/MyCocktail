package com.example.yannous.mycocktail;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewDebug;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yannous.mycocktail.Model.Phone;
import com.example.yannous.mycocktail.Model.PhoneCategory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import iammert.com.expandablelib.ExpandableLayout;
import iammert.com.expandablelib.Section;

public class Activity_frigo extends AppCompatActivity {
    ArrayList<Integer> numlist = new ArrayList<Integer>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frigo);
        //ScrollView sc = (ScrollView)findViewById(R.id.scrollview) ;
        ExpandableLayout layout = (ExpandableLayout)findViewById(R.id.expandable_layout);

        /*sc.setOnTouchsscategoriener(new OnSwipeTouchsscategoriener(getApplicationContext()) {
            public void onSwipeRight() {
                //Intent intent = new Intent(Activity_frigo.this, Activity_frigo.class);
                //startActivity(intent);
                //overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                String a = "";
                for (int i=0 ; i<= numlist.size();i++)
                {
                    a = a + Integer.toString(numlist.get(i));
                }
                Toast.makeText(Activity_frigo.this, "ID selectionner : " + a, Toast.LENGTH_SHORT).show();
            }
        });*/
        Button b = (Button)findViewById(R.id.buttonSearch);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> fcocktail = new ArrayList<>();
                if (numlist.size() != 0) {
                    String maRecherche = BaseDeDonnee.findcocktailwIngredients2(Activity_frigo.this, numlist);
                    if(maRecherche != null)
                    {
                        Intent i = new Intent(Activity_frigo.this,MainActivity.class);
                        i.putExtra("recherche",maRecherche);
                        Log.i("RECHERCHE","Envois de ma recherche");

                        startActivity(i);
                    }
                    else
                    {
                        Toast.makeText(Activity_frigo.this, "Aucun resultat", Toast.LENGTH_SHORT).show();//Dev-----Fct-----
                    }
                }
                else
                {
                    Toast.makeText(Activity_frigo.this, "Aucun ingredient selectionner !", Toast.LENGTH_SHORT).show();
                }
            }
        });
        layout.setRenderer(new ExpandableLayout.Renderer<PhoneCategory, Phone>() {
            @Override
            public void renderParent(View view, PhoneCategory phoneCategory, boolean isExpanded, int parentPosition) {     //Generation categorie principale
                //----------------------------Set nom categorie et affichage arrow graphique-----------------------------------------
                ((TextView)view.findViewById(R.id.tv_parent_name)).setText(phoneCategory.name);
                view.findViewById(R.id.arrow).setBackgroundResource(isExpanded?R.drawable.ic_arrow_up:R.drawable.ic_arrow_down);
            }
            @Override
            public void renderChild(View view, Phone phone, int parentPosition, int childPosition) {                        //generation des sous categorie
                //----------------------------Set nom sous categorie -----------------------------------------
                ((CheckBox)view.findViewById(R.id.tv_child_name)).setText(phone.name);

                //-----------------------------Recuperation d'un click utilisateur checkbox-----------------------------------------
                setOnClick((CheckBox)view.findViewById(R.id.tv_child_name),phone.id);
                //-----------------------------Recuperation de l'ancien etat checkbox et mis a jour visuel--------------------------
                CheckBox c = view.findViewById(R.id.tv_child_name);
                if(!c.isChecked() & Collections.frequency(numlist, phone.id) != 0 )
                {
                    c.setChecked(true);
                }
            }
        });
        //------------------Ajout des differentes sscategories-------------------------------------
        layout.addSection(getalcool());
        layout.addSection(getRhume());
        layout.addSection(getliqueur());
        layout.addSection(getsirop());
        layout.addSection(getjusdefruit());
        layout.addSection(getfruit());
        layout.addSection(getEpiceandsauce());
    }
    private void setOnClick(final CheckBox checkBox, final Integer id){
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                //Toast.makeText(Activity_frigo.this, "Vous avez selectionner l'ingredient ID : " + str  , Toast.LENGTH_SHORT).show();
                CheckBox c = v.findViewById(R.id.tv_child_name);
                //----------------------------Ajout-ID---------------------------------------
                if (c.isChecked())
                {
                    //Toast.makeText(Activity_frigo.this, "Checked"  , Toast.LENGTH_SHORT).show(); //Dev-----Fct-----
                    if (numlist.size() != 0) {
                        if (Collections.frequency(numlist, id) != 0)
                        {
                            return;
                        }
                    }
                    numlist.add(id);
                }
                else
                {
                    //-------------------------Destruction-ID----------------------------------
                    for (int i=0 ; i < numlist.size();i++ )
                    {
                        if(id == numlist.get(i))
                        {
                            //Toast.makeText(Activity_frigo.this, "ID " +numlist.get(i) + "DETRUIT" , Toast.LENGTH_SHORT).show();//Dev-----Fct-----
                            numlist.remove(i);
                            String a = "";
                            /*if (fcocktail.size() != 0) {
                                for (int ia = 0; ia < fcocktail.size(); ia++) {
                                    a += "," + fcocktail.get(ia);
                                }
                                //Toast.makeText(Activity_frigo.this, "Nom sscategorie test : " + a, Toast.LENGTH_LONG).show();//Dev-----Fct-----
                            }
                            */
                            if (numlist.size() != 0) {

                                for (int ia = 1; ia < numlist.size(); ia++) {
                                    a += "," + numlist.get(ia);
                                }
                                //Toast.makeText(Activity_frigo.this, "ID selectionner : " + a, Toast.LENGTH_SHORT).show();//Dev-----Fct-----
                            }
                        }
                    }
                }
            }

        });
    }
    //---------------------categorie----------------------
    private Section<PhoneCategory,Phone> getalcool() {
        Section<PhoneCategory,Phone> section = new Section<>();
        PhoneCategory phoneCategory = new PhoneCategory("Alcool");
        List<Phone> sscategorie = new ArrayList<>();
            sscategorie.add(new Phone("vodka",1));  //1
            sscategorie.add(new Phone("chambord",3));//3
            sscategorie.add(new Phone("tequila",14));
            sscategorie.add(new Phone("champagne",16));
            sscategorie.add(new Phone("cachaça",17));
            sscategorie.add(new Phone("whisky",34));
            sscategorie.add(new Phone("vermouth dry",35));
            sscategorie.add(new Phone("gin",36));
            sscategorie.add(new Phone("vermouth blanc",37));
            sscategorie.add(new Phone("vermouth dry",35));
            sscategorie.add(new Phone("ginger ale (canada dry)",58));
        section.parent = phoneCategory;
        section.children.addAll(sscategorie);
        return section;
    }
    private Section<PhoneCategory,Phone> getRhume() {
        Section<PhoneCategory,Phone> section = new Section<>();
        PhoneCategory phoneCategory = new PhoneCategory("Rhum");
        List<Phone> sscategorie = new ArrayList<>();

            sscategorie.add(new Phone("Rhum cubain",6));
            sscategorie.add(new Phone("Rhum blanc",11));
            sscategorie.add(new Phone("Rhum ambré",12));

        section.parent = phoneCategory;
        section.children.addAll(sscategorie);
        return section;

    }
    private Section<PhoneCategory,Phone> getliqueur() {
        Section<PhoneCategory,Phone> section = new Section<>();
        PhoneCategory phoneCategory = new PhoneCategory("liqueur");
        List<Phone> sscategorie = new ArrayList<>();

        sscategorie.add(new Phone("midori",3));
        sscategorie.add(new Phone("triple sec",15));
        sscategorie.add(new Phone("curaçao bleu",20));
        sscategorie.add(new Phone("liqueur d'abricots",41));
        sscategorie.add(new Phone("liqueur de menthe verte",43));
        sscategorie.add(new Phone("liqueur de menthe blanche",52));
        sscategorie.add(new Phone("liqueur de galliano",54));


        section.parent = phoneCategory;
        section.children.addAll(sscategorie);
        return section;

    }
    private Section<PhoneCategory,Phone> getsirop() {
        Section<PhoneCategory,Phone> section = new Section<>();
        PhoneCategory phoneCategory = new PhoneCategory("sirop");
        List<Phone> sscategorie = new ArrayList<>();

        sscategorie.add(new Phone("sirop de melon",2));
        sscategorie.add(new Phone("sirop sucre de canne",10));
        sscategorie.add(new Phone("sirop de grenadine",23));
        sscategorie.add(new Phone("sirop de menthe",45));
        sscategorie.add(new Phone("sirop d'érable",49));
        sscategorie.add(new Phone("sirop pina colada",55));
        sscategorie.add(new Phone("sirop de fraises",56));

        section.parent = phoneCategory;
        section.children.addAll(sscategorie);
        return section;

    }
    private Section<PhoneCategory,Phone> getjusdefruit() {
        Section<PhoneCategory,Phone> section = new Section<>();
        PhoneCategory phoneCategory = new PhoneCategory("jus de fruit");
        List<Phone> sscategorie = new ArrayList<>();

        sscategorie.add(new Phone("jus d'ananas",4));
        sscategorie.add(new Phone("jus de cranberry",5));
        sscategorie.add(new Phone("jus de citrons verts",7));
        sscategorie.add(new Phone("jus de citrons",21));
        sscategorie.add(new Phone("jus d'oranges",22));
        sscategorie.add(new Phone("jus de pamplemousse",24));
        sscategorie.add(new Phone("jus de tomates",27));
        sscategorie.add(new Phone("jus de pommes",22));
        sscategorie.add(new Phone("jus d'oranges",22));
        sscategorie.add(new Phone("jus d'oranges",22));

        section.parent = phoneCategory;
        section.children.addAll(sscategorie);
        return section;

    }
    private Section<PhoneCategory,Phone> getfruit() {
        Section<PhoneCategory,Phone> section = new Section<>();
        PhoneCategory phoneCategory = new PhoneCategory("fruit");
        List<Phone> sscategorie = new ArrayList<>();

        sscategorie.add(new Phone("citrons verts",18));
        sscategorie.add(new Phone("canelle",25));
        sscategorie.add(new Phone("fraises",59));

        section.parent = phoneCategory;
        section.children.addAll(sscategorie);
        return section;

    }
    private Section<PhoneCategory,Phone> getEpiceandsauce() {
        Section<PhoneCategory,Phone> section = new Section<>();
        PhoneCategory phoneCategory = new PhoneCategory("Epice & sauce");
        List<Phone> sscategorie = new ArrayList<>();

        sscategorie.add(new Phone("sel",31));
        sscategorie.add(new Phone("poivre",32));
        sscategorie.add(new Phone("sucre",19));
        sscategorie.add(new Phone("tabasco",29));
        sscategorie.add(new Phone("sauce worcestershire",28));
        sscategorie.add(new Phone("sucre",19));

        section.parent = phoneCategory;
        section.children.addAll(sscategorie);
        return section;

    }


}
