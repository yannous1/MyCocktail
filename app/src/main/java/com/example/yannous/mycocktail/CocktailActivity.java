package com.example.yannous.mycocktail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class CocktailActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cocktail);

        Intent intent = getIntent();
        String id = intent.getStringExtra("id");

        TextView title = findViewById(R.id.title);
        TextView recette = findViewById(R.id.recette);
        TextView ingredients = findViewById(R.id.ingredients);
        ImageView photo = findViewById(R.id.photo);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.mytoolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ArrayList<String> cocktail = BaseDeDonnee.selectCocktail(this, id);
        title.setText(cocktail.get(0));
        recette.setText(cocktail.get(1));

        String photoStr = cocktail.get(2);
        StringTokenizer tokens = new StringTokenizer(photoStr, ".");
        String photoName = tokens.nextToken();
        int idPhoto = getResources().getIdentifier(photoName, "drawable", getPackageName());
        photo.setImageResource(idPhoto);

        ArrayList<String> ingredientsStr = BaseDeDonnee.selectIngredients(this, id);
        ingredients.setText("• ");
        for(int i = 0; i<ingredientsStr.size(); i++){
            if(i ==  ingredientsStr.size()-1){
                ingredients.setText(ingredients.getText()+ingredientsStr.get(i));
            }
            else{
                ingredients.setText(ingredients.getText()+ingredientsStr.get(i)+"\n• ");
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
