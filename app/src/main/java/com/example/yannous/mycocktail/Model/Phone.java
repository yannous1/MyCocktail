package com.example.yannous.mycocktail.Model;

/**
 * Created by Laurent on 10/02/2018.
 */

public class Phone {
    public String name;
    public int id;

    public Phone(String name , int id) {
        this.name = name;
        this.id = id;
    }
}
