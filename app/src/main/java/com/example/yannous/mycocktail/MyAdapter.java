package com.example.yannous.mycocktail;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {


    private ArrayList<ArrayList<String>> cocktails;

    public  MyAdapter(ArrayList<ArrayList<String>> cocktails)
    {
        this.cocktails = cocktails;
    }

    @Override
    public int getItemCount() {
        return cocktails.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_cell, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ArrayList<String> cocktail = cocktails.get(position);

        holder.name.setText(cocktail.get(1));

        String photoStr = cocktail.get(2);
        StringTokenizer tokens = new StringTokenizer(photoStr, ".");
        String photoName = tokens.nextToken();
        int idPhoto = holder.itemView.getResources().getIdentifier(photoName, "drawable",  getClass().getPackage().getName());
        holder.photo.setImageResource(idPhoto);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(holder.itemView.getContext(), CocktailActivity.class);

                String id = cocktail.get(0);
                intent.putExtra("id", id);

                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView name;
        private final ImageView photo;

        public MyViewHolder(final View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            photo = itemView.findViewById(R.id.photo);
        }
    }
}
