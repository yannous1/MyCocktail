package com.example.yannous.mycocktail;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private static final String DBNAME = "appliCocktail";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String maRecherche = getIntent().getStringExtra("recherche");

        Toolbar toolbar = findViewById(R.id.mytoolbar);
        setSupportActionBar(toolbar);

        final RecyclerView rv = findViewById(R.id.list);
        rv.setLayoutManager(new LinearLayoutManager(this));
        if(maRecherche==null)
        {
            BaseDeDonnee.dbInit(this);
            rv.setAdapter(new MyAdapter(BaseDeDonnee.AllCocktail(this)));
            Log.i("RECHERCHE","marecherche est null");
        }
        if(maRecherche != null)
        {
            rv.setAdapter(new MyAdapter(BaseDeDonnee.RechercheCocktail(this,maRecherche)));
            Log.i("RECHERCHE","marecherche n'est pas null");
        }


        rv.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
            public void onSwipeRight() {
                Intent intent = new Intent(MainActivity.this, Activity_frigo.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            }
        });
    }
}
