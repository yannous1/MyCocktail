package com.example.yannous.mycocktail;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class BaseDeDonnee {

    private static final String DBNAME = "appliCocktail";

    public static void dbInit(Activity activity) {
        SQLiteDatabase db = activity.openOrCreateDatabase(DBNAME,MainActivity.MODE_PRIVATE, null);

        db.execSQL("DROP TABLE IF EXISTS cocktail");
        db.execSQL("DROP TABLE IF EXISTS ingredient");
        db.execSQL("DROP TABLE IF EXISTS compose");

        db.execSQL("CREATE TABLE cocktail (" +
                "    id      INTEGER             NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "    nom  VARCHAR(40)     NOT NULL," +
                "    image  VARCHAR(200)     NOT NULL," +
                "    recette   TEXT     NOT NULL);"
        );
        db.execSQL("CREATE TABLE compose (" +
                "    id      INTEGER             NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "    id_cocktail  INTEGER   NOT NULL," +
                "    id_ingredient  INTEGER   NOT NULL," +
                "CONSTRAINT fk_cocktail_num" +
                "        FOREIGN KEY (id_cocktail) " +
                "        REFERENCES cocktail(id)," +
                "CONSTRAINT fk_ingredient_num" +
                "        FOREIGN KEY (id_ingredient) " +
                "        REFERENCES ingredient(id));"
        );
        db.execSQL("CREATE TABLE ingredient (" +
                "    id      INTEGER             NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "    nom  VARCHAR(40)     NOT NULL);"

        );


        //----------------------------------------------------------------------------------------
        //                                  SEX ON THE BEACH
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail1 = new ContentValues();

        ContentValues vIngredient1 = new ContentValues();
        ContentValues vIngredient2 = new ContentValues();
        ContentValues vIngredient3 = new ContentValues();
        ContentValues vIngredient4 = new ContentValues();
        ContentValues vIngredient5 = new ContentValues();

        ContentValues vCompose1 = new ContentValues();
        ContentValues vCompose2 = new ContentValues();
        ContentValues vCompose3 = new ContentValues();
        ContentValues vCompose4 = new ContentValues();
        ContentValues vCompose5 = new ContentValues();

        vCocktail1.put("nom", "Sex on the beach");
        vCocktail1.put("image", "sex_on_the_beach.jpg");
        vCocktail1.put("recette", "• Réalisez la recette \"Sex on the beach \" dans un verre à mélange. \n" +
                "• Verser les alcools sur des glaçons, mélanger et compléter avec les jus de fruits\n" +
                "\n" +
                "• Servir dans un verre de type \"verre tulipe\"\n" +
                "• Décor: Un morceau d'ananas et une cerise confite.\n" +
                "• Le cocktail souvent imité, jamais égalé. La mûroise peut aisément être remplacée par du Chambord");

        vIngredient1.put("nom", "vodka"); //1
        vIngredient2.put("nom", "sirop melon");//2
        vIngredient3.put("nom", "chambord");//3
        vIngredient4.put("nom", "jus d'ananas");//4
        vIngredient5.put("nom", "jus de cranberry");//5

        vCompose1.put("id_cocktail", 1);
        vCompose1.put("id_ingredient", 1);
        vCompose2.put("id_cocktail", 1);
        vCompose2.put("id_ingredient", 2);
        vCompose3.put("id_cocktail", 1);
        vCompose3.put("id_ingredient", 3);
        vCompose4.put("id_cocktail", 1);
        vCompose4.put("id_ingredient", 4);
        vCompose5.put("id_cocktail", 1);
        vCompose5.put("id_ingredient", 5);

        long resCocktail1 = db.insert("cocktail", null, vCocktail1);

        long resIngredient1 = db.insert("ingredient", null, vIngredient1);
        long resIngredient2 = db.insert("ingredient", null, vIngredient2);
        long resIngredient3 = db.insert("ingredient", null, vIngredient3);
        long resIngredient4 = db.insert("ingredient", null, vIngredient4);
        long resIngredient5 = db.insert("ingredient", null, vIngredient5);

        long resCompose1 = db.insert("compose", null, vCompose1);
        long resCompose2 = db.insert("compose", null, vCompose2);
        long resCompose3 = db.insert("compose", null, vCompose3);
        long resCompose4 = db.insert("compose", null, vCompose4);
        long resCompose5 = db.insert("compose", null, vCompose5);


        Log.d("dbInit", "insert cocktail: " + resCocktail1);

        Log.d("dbInit", "insert ingredient: " + resIngredient1);
        Log.d("dbInit", "insert ingredient: " + resIngredient2);
        Log.d("dbInit", "insert ingredient: " + resIngredient3);
        Log.d("dbInit", "insert ingredient: " + resIngredient4);
        Log.d("dbInit", "insert ingredient: " + resIngredient5);

        Log.d("dbInit", "insert compose: " + resCompose1);
        Log.d("dbInit", "insert compose: " + resCompose2);
        Log.d("dbInit", "insert compose: " + resCompose3);
        Log.d("dbInit", "insert compose: " + resCompose4);
        Log.d("dbInit", "insert compose: " + resCompose5);


        //----------------------------------------------------------------------------------------
        //                                  MOJITO
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail2 = new ContentValues();

        ContentValues vIngredient6 = new ContentValues();
        ContentValues vIngredient7 = new ContentValues();
        ContentValues vIngredient8 = new ContentValues();
        ContentValues vIngredient9 = new ContentValues();
        ContentValues vIngredient10 = new ContentValues();

        ContentValues vCompose6 = new ContentValues();
        ContentValues vCompose7 = new ContentValues();
        ContentValues vCompose8 = new ContentValues();
        ContentValues vCompose9 = new ContentValues();
        ContentValues vCompose10 = new ContentValues();

        vCocktail2.put("nom", "Mojito");
        vCocktail2.put("image", "mojito.jpeg");
        vCocktail2.put("recette"," Réalisez la recette \"Mojito \" directement dans le verre. \n" +
                "• Placer les feuilles de menthe dans le verre, ajoutez le sucre et le jus de citrons. Piler consciencieusement afin d'exprimer l'essence de la menthe mais sans la broyer. Ajouter le rhum, remplir le verre à moitié de glaçons et compléter avec de l'eau gazeuse. Mélanger doucement et servir avec une paille.\n" +
                "• Servir dans un verre de type \"tumbler\"\n" +
                "• Décor: Décorer de feuilles de menthe fraîches et d'une tranche de citron.\n" +
                "• Utilise de préférence du jus de citron vert frais (pressé). Bien que la recette originale ne contienne pas d'angostura, vous pouvez y ajouter quelques gouttes afin de le rendre un peu plus sec.");

        vIngredient6.put("nom", "rhum cubain");//6
        vIngredient7.put("nom", "jus de citrons verts");//7
        vIngredient8.put("nom", "feuille de menthe");//8
        vIngredient9.put("nom", "eau gazeuse");//9
        vIngredient10.put("nom", "sirop sucre de canne");//10

        vCompose6.put("id_cocktail", 2);
        vCompose6.put("id_ingredient", 6);
        vCompose7.put("id_cocktail", 2);
        vCompose7.put("id_ingredient", 7);
        vCompose8.put("id_cocktail", 2);
        vCompose8.put("id_ingredient", 8);
        vCompose9.put("id_cocktail", 2);
        vCompose9.put("id_ingredient", 9);
        vCompose10.put("id_cocktail", 2);
        vCompose10.put("id_ingredient", 10);

        long resCocktail2 = db.insert("cocktail", null, vCocktail2);
        long resIngredient6 = db.insert("ingredient", null, vIngredient6);
        long resIngredient7 = db.insert("ingredient", null, vIngredient7);
        long resIngredient8 = db.insert("ingredient", null, vIngredient8);
        long resIngredient9 = db.insert("ingredient", null, vIngredient9);
        long resIngredient10 = db.insert("ingredient", null, vIngredient10);
        long resCompose6 = db.insert("compose", null, vCompose6);
        long resCompose7 = db.insert("compose", null, vCompose7);
        long resCompose8 = db.insert("compose", null, vCompose8);
        long resCompose9 = db.insert("compose", null, vCompose9);
        long resCompose10 = db.insert("compose", null, vCompose10);

        Log.d("dbInit", "insert cocktail: " + resCocktail2);
        Log.d("dbInit", "insert ingredient: " + resIngredient6);
        Log.d("dbInit", "insert ingredient: " + resIngredient7);
        Log.d("dbInit", "insert ingredient: " + resIngredient8);
        Log.d("dbInit", "insert ingredient: " + resIngredient9);
        Log.d("dbInit", "insert ingredient: " + resIngredient10);
        Log.d("dbInit", "insert compose: " + resCompose6);
        Log.d("dbInit", "insert compose: " + resCompose7);
        Log.d("dbInit", "insert compose: " + resCompose8);
        Log.d("dbInit", "insert compose: " + resCompose9);
        Log.d("dbInit", "insert compose: " + resCompose10);

        //----------------------------------------------------------------------------------------
        //                                  PINA COLADA
        //----------------------------------------------------------------------------------------

        ContentValues vCocktail3 = new ContentValues();

        ContentValues vIngredient11 = new ContentValues();
        ContentValues vIngredient12 = new ContentValues();
        ContentValues vIngredient13 = new ContentValues();

        ContentValues vCompose11 = new ContentValues();
        ContentValues vCompose12 = new ContentValues();
        ContentValues vCompose13 = new ContentValues();
        ContentValues vCompose14 = new ContentValues();



        vCocktail3.put("nom", "Pina Colada");
        vCocktail3.put("image", "pina_colada.jpg");
        vCocktail3.put("recette"," • Réalisez la recette \"Piña Colada \" au mixer. \n" +
                "• Dans un blender (mixer), versez les ingrédients avec 5 ou 6 glaçons et mixez le tout. C'est prêt ! Versez dans le verre et dégustez. Peut aussi se réaliser au shaker si c'est juste pour une personne.\n" +
                "• Servir dans un verre de type \"verre à vin\"\n" +
                "• Décor: Décorer avec un morceau d'ananas et une cerise confite.\n" +
                "• Vous pouvez ajouter une touche d'onctuosité en ajoutant une cuillère à soupe de crème fraîche dans le mixer.");

        vIngredient11.put("nom", "rhum blanc");//11
        vIngredient12.put("nom", "rhum ambré");//12
        // vIngredient2.put("nom", "jus d'ananas");//4
        vIngredient13.put("nom", "lait de coco");//13

        vCompose11.put("id_cocktail", 3);
        vCompose11.put("id_ingredient", 11);
        vCompose12.put("id_cocktail", 3);
        vCompose12.put("id_ingredient", 12);
        vCompose13.put("id_cocktail", 3);
        vCompose13.put("id_ingredient", 4);
        vCompose14.put("id_cocktail", 3);
        vCompose14.put("id_ingredient", 13);

        long resCocktail3 = db.insert("cocktail", null, vCocktail3);

        long resIngredient11 = db.insert("ingredient", null, vIngredient11);
        long resIngredient12 = db.insert("ingredient", null, vIngredient12);
        long resIngredient13 = db.insert("ingredient", null, vIngredient13);

        long resCompose11 = db.insert("compose", null, vCompose11);
        long resCompose12 = db.insert("compose", null, vCompose12);
        long resCompose13 = db.insert("compose", null, vCompose13);
        long resCompose14 = db.insert("compose", null, vCompose14);

        Log.d("dbInit", "insert cocktail: " + resCocktail3);

        Log.d("dbInit", "insert ingredient: " + resIngredient11);
        Log.d("dbInit", "insert ingredient: " + resIngredient12);
        Log.d("dbInit", "insert ingredient: " + resIngredient13);

        Log.d("dbInit", "insert compose: " + resCompose11);
        Log.d("dbInit", "insert compose: " + resCompose12);
        Log.d("dbInit", "insert compose: " + resCompose13);
        Log.d("dbInit", "insert compose: " + resCompose14);

        //----------------------------------------------------------------------------------------
        //                                  MARGARITA
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail4 = new ContentValues();
        ContentValues vIngredient14 = new ContentValues();
        ContentValues vIngredient15 = new ContentValues();
        ContentValues vCompose15 = new ContentValues();
        ContentValues vCompose16 = new ContentValues();
        ContentValues vCompose17 = new ContentValues();

        vCocktail4.put("nom", "Margarita");
        vCocktail4.put("image", "margarita.jpg");
        vCocktail4.put("recette","Réalisez la recette \"Margarita \" au shaker. \n" +
                "• Frapper les ingrédients au shaker avec des glaçons puis verser dans le verre givré au citron et au sel fin...\n" +
                "\n" +
                "Pour givrer facilement le verre, passer le citron sur le bord du verre et tremper les bords dans le sel.\n" +
                "• Servir dans un verre de type \"verre à margarita\"\n" +
                "• Décor: Décorer d'une tranche de citron vert...");

        vIngredient14.put("nom", "tequila");//14
        vIngredient15.put("nom", "triple sec");//15
        //vIngredient.put("nom", "jus de citrons verts");//7

        vCompose15.put("id_cocktail", 4);
        vCompose15.put("id_ingredient", 14);
        vCompose16.put("id_cocktail", 4);
        vCompose16.put("id_ingredient", 15);
        vCompose17.put("id_cocktail",4);
        vCompose17.put("id_ingredient", 7);

        long resCocktail4 = db.insert("cocktail", null, vCocktail4);

        long resIngredient14 = db.insert("ingredient", null, vIngredient14);
        long resIngredient15 = db.insert("ingredient", null, vIngredient15);

        long resCompose15 = db.insert("compose", null, vCompose15);
        long resCompose16 = db.insert("compose", null, vCompose16);
        long resCompose17 = db.insert("compose", null, vCompose17);


        Log.d("dbInit", "insert cocktail: " + resCocktail4);

        Log.d("dbInit", "insert ingredient: " + resIngredient14);
        Log.d("dbInit", "insert ingredient: " + resIngredient15);

        Log.d("dbInit", "insert compose: " + resCompose15);
        Log.d("dbInit", "insert compose: " + resCompose16);
        Log.d("dbInit", "insert compose: " + resCompose17);

        //----------------------------------------------------------------------------------------
        //                                  SOUPE DE CHAMPAGNE
        //----------------------------------------------------------------------------------------

        ContentValues vCocktail5 = new ContentValues();
        ContentValues vIngredient16 = new ContentValues();
        ContentValues vCompose18 = new ContentValues();
        ContentValues vCompose19 = new ContentValues();
        ContentValues vCompose20 = new ContentValues();
        ContentValues vCompose21 = new ContentValues();

        vCocktail5.put("nom", "Soupe de champagne");
        vCocktail5.put("image", "soupe_de_champagne.jpg");
        vCocktail5.put("recette"," Réalisez la recette \"Soupe de Champagne \" dans un verre à mélange. \n" +
                "• Préparation au dernier moment.\n" +
                "L'ensemble des ingrédients doit être stocké au frais.\n" +
                "Mélanger doucement les ingrédients dans une grande coupe à punch ou dans un saladier. Servez à la louche.\n" +
                "• Servir dans un verre de type \"flûte\"");

        //vIngredient4.put("nom", "triple sec");//15
        vIngredient16.put("nom", "champagne");//16
        //vIngredient4.put("nom", "jus de citrons verts");//7
        //vIngredient4.put("nom", "sirop sucre de canne");//10

        vCompose18.put("id_cocktail", 5);
        vCompose18.put("id_ingredient", 15);
        vCompose19.put("id_cocktail", 5);
        vCompose19.put("id_ingredient", 16);
        vCompose20.put("id_cocktail", 5);
        vCompose20.put("id_ingredient", 7);
        vCompose21.put("id_cocktail", 5);
        vCompose21.put("id_ingredient", 10);

        long resCocktail5 = db.insert("cocktail", null, vCocktail5);
        long resIngredient16 = db.insert("ingredient", null, vIngredient16);
        long resCompose18 = db.insert("compose", null, vCompose18);
        long resCompose19 = db.insert("compose", null, vCompose19);
        long resCompose20 = db.insert("compose", null, vCompose20);
        long resCompose21 = db.insert("compose", null, vCompose21);

        Log.d("dbInit", "insert cocktail: " + resCocktail5);

        Log.d("dbInit", "insert ingredient: " + resIngredient16);

        Log.d("dbInit", "insert compose: " + resCompose18);
        Log.d("dbInit", "insert compose: " + resCompose19);
        Log.d("dbInit", "insert compose: " + resCompose20);
        Log.d("dbInit", "insert compose: " + resCompose21);

        //----------------------------------------------------------------------------------------
        //                                  CAIPIRINHA
        //----------------------------------------------------------------------------------------

        ContentValues vCocktail6 = new ContentValues();
        ContentValues vIngredient17 = new ContentValues();
        ContentValues vIngredient18 = new ContentValues();
        ContentValues vIngredient19 = new ContentValues();
        ContentValues vCompose22 = new ContentValues();
        ContentValues vCompose23 = new ContentValues();
        ContentValues vCompose24 = new ContentValues();

        vCocktail6.put("nom", "Caipirinha");
        vCocktail6.put("image", "caipirinha.jpg");
        vCocktail6.put("recette"," Réalisez la recette \"Caipirinha \" directement dans le verre. \n" +
                "• Lavez le citron vert et coupez les deux extrémités. Coupez le citron en 8 ou 9 morceaux et retirez la partie blanche centrale responsable de l'amertume. Placez les morceaux dans le verre et versez une bonne cuillère à soupe de sucre. Pilez fermement le tout dans le verre avec le sucre en poudre jusqu'à l'extraction la plus complète possible du jus. Recouvrir le mélange citron-sucre d'une bonne couche de glace pilée, concassée ou de glaçons simples (ras bord), puis faire le niveau à la cachaça jusqu'à un doigt du bord. \n" +
                "\n" +
                "Ne pas rajouter de sucre après l'adjonction de la glace (le nouveau sucre ne se dissout plus). Mélanger legèrement avec un mélangeur et servir avec une ou deux petites pailles (au cas où l'une soit bouchée par la glace ou la pulpe du citron).\n" +
                "• Servir dans un verre de type \"old fashioned\"");

        vIngredient17.put("nom", "cachaça");//17
        vIngredient18.put("nom", "citrons verts");//18
        vIngredient19.put("nom", "sucre");//19

        vCompose22.put("id_cocktail", 6);
        vCompose22.put("id_ingredient", 17);
        vCompose23.put("id_cocktail", 6);
        vCompose23.put("id_ingredient", 18);
        vCompose24.put("id_cocktail", 6);
        vCompose24.put("id_ingredient", 19);

        long resCocktail6 = db.insert("cocktail", null, vCocktail6);

        long resIngredient17 = db.insert("ingredient", null, vIngredient17);
        long resIngredient18 = db.insert("ingredient", null, vIngredient18);
        long resIngredient19 = db.insert("ingredient", null, vIngredient19);

        long resCompose22 = db.insert("compose", null, vCompose22);
        long resCompose23 = db.insert("compose", null, vCompose23);
        long resCompose24 = db.insert("compose", null, vCompose24);

        Log.d("dbInit", "insert cocktail: " + resCocktail6);

        Log.d("dbInit", "insert ingredient: " + resIngredient17);
        Log.d("dbInit", "insert ingredient: " + resIngredient18);
        Log.d("dbInit", "insert ingredient: " + resIngredient19);

        Log.d("dbInit", "insert compose: " + resCompose22);
        Log.d("dbInit", "insert compose: " + resCompose23);
        Log.d("dbInit", "insert compose: " + resCompose24);

        //----------------------------------------------------------------------------------------
        //                                  COSMOPOLITAN
        //----------------------------------------------------------------------------------------

        ContentValues vCocktail7 = new ContentValues();
        //ContentValues vIngredient = new ContentValues();
        ContentValues vCompose25 = new ContentValues();
        ContentValues vCompose26 = new ContentValues();
        ContentValues vCompose27 = new ContentValues();
        ContentValues vCompose28 = new ContentValues();

        vCocktail7.put("nom", "Cosmopolitan");
        vCocktail7.put("image", "cosmopolitan.jpg");
        vCocktail7.put("recette"," Réalisez la recette \"Cosmopolitan \" au shaker. \n" +
                "• Frapper les ingrédients avec des glaçons et verser dans le verre en filtrant.\n" +
                "• Servir dans un verre de type \"verre à martini\"\n" +
                "• Décor: Eventuellement une rondelle de citron sur le bord du verre.\n" +
                "• Pour un cosmopolitan plus caractériel, remplacer le jus de cranberry par 1 cl de sirop de cranberry.");

        //vIngredient.put("nom", "vodka");//1
        //vIngredient.put("nom", "triple sec");//15
        //vIngredient.put("nom", "jus de cranberry");//5
        //vIngredient.put("nom", "jus de citrons verts");//7

        vCompose25.put("id_cocktail", 7);
        vCompose25.put("id_ingredient", 1);
        vCompose26.put("id_cocktail", 7);
        vCompose26.put("id_ingredient", 15);
        vCompose27.put("id_cocktail", 7);
        vCompose27.put("id_ingredient", 15);
        vCompose28.put("id_cocktail", 7);
        vCompose28.put("id_ingredient", 7);

        long resCocktail7 = db.insert("cocktail", null, vCocktail7);
        //long resIngredient6 = db.insert("ingredient", null, vIngredient6);
        long resCompose25 = db.insert("compose", null, vCompose25);
        long resCompose26 = db.insert("compose", null, vCompose26);
        long resCompose27 = db.insert("compose", null, vCompose27);
        long resCompose28 = db.insert("compose", null, vCompose28);

        Log.d("dbInit", "insert cocktail: " + resCocktail7);
        //Log.d("dbInit", "insert ingredient: " + resIngredient6);
        Log.d("dbInit", "insert compose: " + resCompose25);
        Log.d("dbInit", "insert compose: " + resCompose26);
        Log.d("dbInit", "insert compose: " + resCompose27);
        Log.d("dbInit", "insert compose: " + resCompose28);

        //----------------------------------------------------------------------------------------
        //                                  BLUE LAGOON
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail8 = new ContentValues();
        ContentValues vIngredient20 = new ContentValues();
        ContentValues vIngredient21 = new ContentValues();
        ContentValues vCompose29 = new ContentValues();
        ContentValues vCompose30 = new ContentValues();
        ContentValues vCompose31 = new ContentValues();

        vCocktail8.put("nom", "Blue lagoon");
        vCocktail8.put("image", "blue_lagoon.png");
        vCocktail8.put("recette"," Réalisez la recette \"Blue Lagoon \" au shaker. \n" +
                "• Pressez le jus d'un demi-citron, ajoutez dans le shaker avec les autres ingrédients et des glaçons. Frappez puis versez dans le verre en filtrant.\n" +
                "\n" +
                "Afin qu'il soit plus frais et léger, remplissez auparavant le verre de glace pilée.\n" +
                "• Servir dans un verre de type \"verre à martini\"\n" +
                "• Décor: Un long zeste de citron vert.");

        //vIngredient.put("nom", "vodka");//1
        vIngredient20.put("nom", "curaçao bleu");//20
        vIngredient21.put("nom", "jus de citrons");//21

        vCompose29.put("id_cocktail", 8);
        vCompose29.put("id_ingredient", 1);
        vCompose30.put("id_cocktail", 8);
        vCompose30.put("id_ingredient", 20);
        vCompose31.put("id_cocktail", 8);
        vCompose31.put("id_ingredient", 21);

        long resCocktail8 = db.insert("cocktail", null, vCocktail8);

        long resIngredient20 = db.insert("ingredient", null, vIngredient20);
        long resIngredient21 = db.insert("ingredient", null, vIngredient21);

        long resCompose29 = db.insert("compose", null, vCompose29);
        long resCompose30 = db.insert("compose", null, vCompose30);
        long resCompose31 = db.insert("compose", null, vCompose31);

        Log.d("dbInit", "insert cocktail: " + resCocktail8);
        Log.d("dbInit", "insert ingredient: " + resIngredient20);
        Log.d("dbInit", "insert ingredient: " + resIngredient21);
        Log.d("dbInit", "insert compose: " + resCompose29);
        Log.d("dbInit", "insert compose: " + resCompose30);
        Log.d("dbInit", "insert compose: " + resCompose31);

        //----------------------------------------------------------------------------------------
        //                                  TEQUILA SUNRISE
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail9 = new ContentValues();
        ContentValues vIngredient22 = new ContentValues();
        ContentValues vIngredient23 = new ContentValues();
        ContentValues vCompose32 = new ContentValues();
        ContentValues vCompose33 = new ContentValues();
        ContentValues vCompose34 = new ContentValues();

        vCocktail9.put("nom", "Tequila sunrise");
        vCocktail9.put("image", "tequila_sunrise.jpg");
        vCocktail9.put("recette","Réalisez la recette \"Tequila Sunrise \" directement dans le verre. \n" +
                "• Verser la tequila sur des glaçons dans le verre. Compléter avec le jus d'orange et remuer. Versez doucement le sirop de grenadine dans le verre pour que celui-ci tombe au fond. Donnez alors un petit coup de cuillère pour affiner le dégradé si nécessaire. \n" +
                "• Servir dans un verre de type \"tumbler\"\n" +
                "• Décor: Une rondelle d'orange.");

        //vIngredient.put("nom", "tequila");//14
        vIngredient22.put("nom", "jus d'oranges");//22
        vIngredient23.put("nom", "sirop de grenadine");//23

        vCompose32.put("id_cocktail", 9);
        vCompose32.put("id_ingredient", 14);
        vCompose33.put("id_cocktail", 9);
        vCompose33.put("id_ingredient", 22);
        vCompose34.put("id_cocktail", 9);
        vCompose34.put("id_ingredient", 23);

        long resCocktail9 = db.insert("cocktail", null, vCocktail9);

        long resIngredient22 = db.insert("ingredient", null, vIngredient22);
        long resIngredient23 = db.insert("ingredient", null, vIngredient23);

        long resCompose32 = db.insert("compose", null, vCompose32);
        long resCompose33 = db.insert("compose", null, vCompose33);
        long resCompose34 = db.insert("compose", null, vCompose34);

        Log.d("dbInit", "insert cocktail: " + resCocktail9);
        Log.d("dbInit", "insert ingredient: " + resIngredient22);
        Log.d("dbInit", "insert ingredient: " + resIngredient23);
        Log.d("dbInit", "insert compose: " + resCompose32);
        Log.d("dbInit", "insert compose: " + resCompose33);
        Log.d("dbInit", "insert compose: " + resCompose34);

        //----------------------------------------------------------------------------------------
        //                                  PUNCH
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail10 = new ContentValues();
        ContentValues vIngredient24 = new ContentValues();
        ContentValues vIngredient25 = new ContentValues();
        ContentValues vIngredient26 = new ContentValues();
        ContentValues vCompose35 = new ContentValues();
        ContentValues vCompose36 = new ContentValues();
        ContentValues vCompose37 = new ContentValues();
        ContentValues vCompose38 = new ContentValues();
        ContentValues vCompose39 = new ContentValues();
        ContentValues vCompose40 = new ContentValues();
        ContentValues vCompose41 = new ContentValues();

        vCocktail10.put("nom", "Punch");
        vCocktail10.put("image", "punch.jpg");
        vCocktail10.put("recette","Réalisez la recette \"Punch \" directement en bouteilles. \n" +
                "• Faire infuser 3 sachets de thé et un bâton de cannelle dans un verre d'eau bouillante pendant 5 minutes.\n" +
                "\n" +
                "Si vous n'avez pas de bâtons de cannelle, utilisez du sirop de cannelle ou saupoudrez de cannelle en poudre.\n" +
                "\n" +
                "Ensuite, mélanger le tout dans une grande bouteille à eau de 5 l, très pratique pour mettre au réfrigérateur. Ou dans un saladier, pour servir avec une louche. A servir frais.\n" +
                "• Utiliser de préférence du thé d'Inde non aromatisé ou de Ceylan.");

        //vIngredient.put("nom", "rhum blanc");//11
        //vIngredient.put("nom", "sirop sucre de canne");//10
        //vIngredient.put("nom", "jus d'oranges");//22
        //vIngredient.put("nom", "jus d'ananas");//4
        vIngredient24.put("nom", "jus de pamplemousse");//24
        vIngredient25.put("nom", "canelle");//25
        vIngredient26.put("nom", "thé");//26

        vCompose35.put("id_cocktail", 10);
        vCompose35.put("id_ingredient", 11);
        vCompose36.put("id_cocktail", 10);
        vCompose36.put("id_ingredient", 10);
        vCompose37.put("id_cocktail", 10);
        vCompose37.put("id_ingredient", 22);
        vCompose38.put("id_cocktail", 10);
        vCompose38.put("id_ingredient", 4);
        vCompose39.put("id_cocktail", 10);
        vCompose39.put("id_ingredient", 24);
        vCompose40.put("id_cocktail", 10);
        vCompose40.put("id_ingredient", 25);
        vCompose41.put("id_cocktail", 10);
        vCompose41.put("id_ingredient", 26);

        long resCocktail10 = db.insert("cocktail", null, vCocktail10);
        long resIngredient24 = db.insert("ingredient", null, vIngredient24);
        long resIngredient25 = db.insert("ingredient", null, vIngredient25);
        long resIngredient26 = db.insert("ingredient", null, vIngredient26);
        long resCompose35 = db.insert("compose", null, vCompose35);
        long resCompose36 = db.insert("compose", null, vCompose36);
        long resCompose37 = db.insert("compose", null, vCompose37);
        long resCompose38 = db.insert("compose", null, vCompose38);
        long resCompose39 = db.insert("compose", null, vCompose39);
        long resCompose40 = db.insert("compose", null, vCompose40);
        long resCompose41 = db.insert("compose", null, vCompose41);

        Log.d("dbInit", "insert cocktail: " + resCocktail10);
        Log.d("dbInit", "insert ingredient: " + resIngredient24);
        Log.d("dbInit", "insert ingredient: " + resIngredient25);
        Log.d("dbInit", "insert ingredient: " + resIngredient26);
        Log.d("dbInit", "insert compose: " + resCompose35);
        Log.d("dbInit", "insert compose: " + resCompose36);
        Log.d("dbInit", "insert compose: " + resCompose37);
        Log.d("dbInit", "insert compose: " + resCompose38);
        Log.d("dbInit", "insert compose: " + resCompose39);
        Log.d("dbInit", "insert compose: " + resCompose40);
        Log.d("dbInit", "insert compose: " + resCompose41);

        //----------------------------------------------------------------------------------------
        //                                  BLOODY MARY
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail11 = new ContentValues();
        ContentValues vIngredient27 = new ContentValues();
        ContentValues vIngredient28 = new ContentValues();
        ContentValues vIngredient29 = new ContentValues();
        ContentValues vIngredient30 = new ContentValues();
        ContentValues vIngredient31 = new ContentValues();
        ContentValues vIngredient32 = new ContentValues();
        ContentValues vCompose42 = new ContentValues();
        ContentValues vCompose43 = new ContentValues();
        ContentValues vCompose44 = new ContentValues();
        ContentValues vCompose45 = new ContentValues();
        ContentValues vCompose46 = new ContentValues();
        ContentValues vCompose47 = new ContentValues();
        ContentValues vCompose48 = new ContentValues();
        ContentValues vCompose49 = new ContentValues();

        vCocktail11.put("nom", "Bloody Mary");
        vCocktail11.put("image", "bloody_mary.jpg");
        vCocktail11.put("recette","Réalisez la recette \"Bloody Mary \" dans un verre à mélange. \n" +
                "• Agiter les ingrédients dans un verre à mélange avec des glaçons (pour refroidir sans trop diluer). Verser dans le verre, puis ajouter à convenance sel de céleri, sel et poivre. Décorer avec une tige de céleri et optionellement, une rondelle de citron.\n" +
                "Servir. \n" +
                "• Servir dans un verre de type \"tumbler\"\n" +
                "• Décor: Décorer d'une branche de celeri.\n" +
                "• Il est extrêmement difficile de trouver un Bloody Mary convenablement préparé, mais le jour où l'on en boit un \"vrai\", généralement on l'adopte... nous garantissons que la recette présentée ici est l'originale!");

        //vIngredient.put("nom", "vodka");//1
        vIngredient27.put("nom", "jus de tomates");//27
        //vIngredient.put("nom", "jus de citrons");//21
        vIngredient28.put("nom", " sauce worcestershire");//28
        vIngredient29.put("nom", "tabasco");//29
        vIngredient30.put("nom", "sel de céleri");//30
        vIngredient31.put("nom", "sel");//31
        vIngredient32.put("nom", "poivre");//32

        vCompose42.put("id_cocktail", 11);
        vCompose42.put("id_ingredient", 1);
        vCompose43.put("id_cocktail", 11);
        vCompose43.put("id_ingredient", 27);
        vCompose44.put("id_cocktail", 11);
        vCompose44.put("id_ingredient", 21);
        vCompose45.put("id_cocktail", 11);
        vCompose45.put("id_ingredient", 28);
        vCompose46.put("id_cocktail", 11);
        vCompose46.put("id_ingredient", 29);
        vCompose47.put("id_cocktail", 11);
        vCompose47.put("id_ingredient", 30);
        vCompose48.put("id_cocktail", 11);
        vCompose48.put("id_ingredient", 31);
        vCompose49.put("id_cocktail", 11);
        vCompose49.put("id_ingredient", 32);

        long resCocktail11 = db.insert("cocktail", null, vCocktail11);
        long resIngredient27 = db.insert("ingredient", null, vIngredient27);
        long resIngredient28 = db.insert("ingredient", null, vIngredient28);
        long resIngredient29 = db.insert("ingredient", null, vIngredient29);
        long resIngredient30 = db.insert("ingredient", null, vIngredient30);
        long resIngredient31 = db.insert("ingredient", null, vIngredient31);
        long resIngredient32 = db.insert("ingredient", null, vIngredient32);
        long resCompose42 = db.insert("compose", null, vCompose42);
        long resCompose43 = db.insert("compose", null, vCompose43);
        long resCompose44 = db.insert("compose", null, vCompose44);
        long resCompose45 = db.insert("compose", null, vCompose45);
        long resCompose46 = db.insert("compose", null, vCompose46);
        long resCompose47 = db.insert("compose", null, vCompose47);
        long resCompose48 = db.insert("compose", null, vCompose48);
        long resCompose49 = db.insert("compose", null, vCompose49);

        Log.d("dbInit", "insert cocktail: " + resCocktail11);
        Log.d("dbInit", "insert ingredient: " + resIngredient27);
        Log.d("dbInit", "insert ingredient: " + resIngredient28);
        Log.d("dbInit", "insert ingredient: " + resIngredient29);
        Log.d("dbInit", "insert ingredient: " + resIngredient30);
        Log.d("dbInit", "insert ingredient: " + resIngredient31);
        Log.d("dbInit", "insert ingredient: " + resIngredient32);
        Log.d("dbInit", "insert compose: " + resCompose42);
        Log.d("dbInit", "insert compose: " + resCompose43);
        Log.d("dbInit", "insert compose: " + resCompose44);
        Log.d("dbInit", "insert compose: " + resCompose45);
        Log.d("dbInit", "insert compose: " + resCompose46);
        Log.d("dbInit", "insert compose: " + resCompose47);
        Log.d("dbInit", "insert compose: " + resCompose48);
        Log.d("dbInit", "insert compose: " + resCompose49);

        //----------------------------------------------------------------------------------------
        //                                  CUBA LIBRE
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail12 = new ContentValues();
        ContentValues vIngredient33 = new ContentValues();
        ContentValues vCompose50 = new ContentValues();
        ContentValues vCompose51 = new ContentValues();
        ContentValues vCompose52 = new ContentValues();

        vCocktail12.put("nom", "Cuba Libre");
        vCocktail12.put("image", "cuba_libre.jpg");
        vCocktail12.put("recette"," Réalisez la recette \"Cuba libre \" directement dans le verre. \n" +
                "• Verser citron et rhum sur des glaçons. Compléter avec le coca cola. Remuer lentement.\n" +
                "• Servir dans un verre de type \"tumbler\"\n" +
                "• Décor: Décorer avec une tranche de citron vert\n" +
                "• Très en vogue. Simple et prestigieux.");

        //vIngredient.put("nom", "rhum cubain");//6
        //vIngredient.put("nom", "jus de citrons verts");//7
        vIngredient33.put("nom", "cola");//33

        vCompose50.put("id_cocktail", 12);
        vCompose50.put("id_ingredient", 6);
        vCompose51.put("id_cocktail", 12);
        vCompose51.put("id_ingredient", 7);
        vCompose52.put("id_cocktail", 12);
        vCompose52.put("id_ingredient", 33);

        long resCocktail12 = db.insert("cocktail", null, vCocktail12);
        long resIngredient33 = db.insert("ingredient", null, vIngredient33);
        long resCompose50 = db.insert("compose", null, vCompose50);
        long resCompose51 = db.insert("compose", null, vCompose51);
        long resCompose52 = db.insert("compose", null, vCompose52);

        Log.d("dbInit", "insert cocktail: " + resCocktail12);
        Log.d("dbInit", "insert ingredient: " + resIngredient33);
        Log.d("dbInit", "insert compose: " + resCompose50);
        Log.d("dbInit", "insert compose: " + resCompose51);
        Log.d("dbInit", "insert compose: " + resCompose52);

        //----------------------------------------------------------------------------------------
        //                                  42ND STREET
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail13 = new ContentValues();
        ContentValues vIngredient34 = new ContentValues();
        ContentValues vIngredient35 = new ContentValues();
        ContentValues vCompose53 = new ContentValues();
        ContentValues vCompose54 = new ContentValues();
        ContentValues vCompose55 = new ContentValues();

        vCocktail13.put("nom", "42nd Street");
        vCocktail13.put("image", "nd_street.jpg");
        vCocktail13.put("recette","Réalisez la recette \"42nd Street \" au shaker. \n" +
                "• Frapper au shaker avec des glaçons versez dans le verre. \n" +
                "• Servir dans un verre de type \"verre à martini\"\n" +
                "• Décor: Décorez d'un zeste de citron et une cerise au parasquin piqués.");

        vIngredient34.put("nom", "whisky");//34
        //vIngredient.put("nom", "triple sec");//15
        vIngredient35.put("nom", "vermouth dry");//35

        vCompose53.put("id_cocktail", 13);
        vCompose53.put("id_ingredient", 34);
        vCompose54.put("id_cocktail", 13);
        vCompose54.put("id_ingredient", 15);
        vCompose55.put("id_cocktail", 13);
        vCompose55.put("id_ingredient", 35);

        long resCocktail13 = db.insert("cocktail", null, vCocktail13);
        long resIngredient34 = db.insert("ingredient", null, vIngredient34);
        long resIngredient35 = db.insert("ingredient", null, vIngredient35);
        long resCompose53 = db.insert("compose", null, vCompose53);
        long resCompose54 = db.insert("compose", null, vCompose54);
        long resCompose55 = db.insert("compose", null, vCompose55);

        Log.d("dbInit", "insert cocktail: " + resCocktail13);
        Log.d("dbInit", "insert ingredient: " + resIngredient34);
        Log.d("dbInit", "insert ingredient: " + resIngredient35);
        Log.d("dbInit", "insert compose: " + resCompose53);
        Log.d("dbInit", "insert compose: " + resCompose54);
        Log.d("dbInit", "insert compose: " + resCompose55);

        //----------------------------------------------------------------------------------------
        //                                  LONG ISLAND ICE TEA
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail14 = new ContentValues();
        ContentValues vIngredient36 = new ContentValues();
        ContentValues vCompose56 = new ContentValues();
        ContentValues vCompose57 = new ContentValues();
        ContentValues vCompose58 = new ContentValues();
        ContentValues vCompose59 = new ContentValues();
        ContentValues vCompose60 = new ContentValues();
        ContentValues vCompose61 = new ContentValues();
        ContentValues vCompose62 = new ContentValues();

        vCocktail14.put("nom", "Long Island Ice Tea");
        vCocktail14.put("image", "long_island_ice_tea.jpg");
        vCocktail14.put("recette"," Réalisez la recette \"Long island Iced Tea \" au shaker. \n" +
                "• Frapper tous les ingrédients sauf le cola au shaker avec quelques glaçons, et verser (en retenant les glaçons) dans le verre contenant des glaçons. Complétez avec le cola.\n" +
                "• Servir dans un verre de type \"tumbler\"\n" +
                "• Décor: Décorer avec une tranche de citron.\n" +
                "• LIIT pour les intimes.");

        //vIngredient.put("nom", "vodka");//1
        vIngredient36.put("nom", "gin");//36
        //vIngredient.put("nom", "rhum blanc");//11
        //vIngredient.put("nom", "triple sec");//15
        //vIngredient.put("nom", "tequila");//14
        //vIngredient.put("nom", "jus de citrons");//21
        //vIngredient.put("nom", "cola");//33

        vCompose56.put("id_cocktail", 14);
        vCompose56.put("id_ingredient", 1);
        vCompose57.put("id_cocktail", 14);
        vCompose57.put("id_ingredient", 36);
        vCompose58.put("id_cocktail", 14);
        vCompose58.put("id_ingredient", 11);
        vCompose59.put("id_cocktail", 14);
        vCompose59.put("id_ingredient", 15);
        vCompose60.put("id_cocktail", 14);
        vCompose60.put("id_ingredient", 14);
        vCompose61.put("id_cocktail", 14);
        vCompose61.put("id_ingredient", 21);
        vCompose62.put("id_cocktail", 14);
        vCompose62.put("id_ingredient", 33);

        long resCocktail14 = db.insert("cocktail", null, vCocktail14);
        long resIngredient36 = db.insert("ingredient", null, vIngredient36);
        long resCompose56 = db.insert("compose", null, vCompose56);
        long resCompose57 = db.insert("compose", null, vCompose57);
        long resCompose58 = db.insert("compose", null, vCompose58);
        long resCompose59 = db.insert("compose", null, vCompose59);
        long resCompose60 = db.insert("compose", null, vCompose60);
        long resCompose61 = db.insert("compose", null, vCompose61);
        long resCompose62 = db.insert("compose", null, vCompose62);

        Log.d("dbInit", "insert cocktail: " + resCocktail14);
        Log.d("dbInit", "insert ingredient: " + resIngredient36);
        Log.d("dbInit", "insert compose: " + resCompose56);
        Log.d("dbInit", "insert compose: " + resCompose57);
        Log.d("dbInit", "insert compose: " + resCompose58);
        Log.d("dbInit", "insert compose: " + resCompose59);
        Log.d("dbInit", "insert compose: " + resCompose60);
        Log.d("dbInit", "insert compose: " + resCompose61);
        Log.d("dbInit", "insert compose: " + resCompose62);

        //----------------------------------------------------------------------------------------
        //                                  HAMPTON
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail15 = new ContentValues();
        ContentValues vIngredient37 = new ContentValues();
        ContentValues vCompose63 = new ContentValues();
        ContentValues vCompose64 = new ContentValues();
        ContentValues vCompose65 = new ContentValues();
        ContentValues vCompose66 = new ContentValues();

        vCocktail15.put("nom", "Hampton");
        vCocktail15.put("image", "hampton.jpg");
        vCocktail15.put("recette"," Réalisez la recette \"Hampton \" directement dans le verre. \n" +
                "• Verser le Martini, le gin et le whisky dans une flûte. \n" +
                "Compléter avec du champagne bien frappé. \n" +
                "• Servir dans un verre de type \"flûte\"\n" +
                "• Ce cocktail tirant son nom du musicien Lionel Hampton car il puisait son inspiration dans le \"champagne-bourbon\"");

        vIngredient37.put("nom", "vermouth blanc");//37
        //vIngredient.put("nom", "gin");//36
        //vIngredient.put("nom", "whisky");//34
        //vIngredient.put("nom", "champagne");//16

        vCompose63.put("id_cocktail", 15);
        vCompose63.put("id_ingredient", 37);
        vCompose64.put("id_cocktail", 15);
        vCompose64.put("id_ingredient", 36);
        vCompose65.put("id_cocktail", 15);
        vCompose65.put("id_ingredient", 34);
        vCompose66.put("id_cocktail", 15);
        vCompose66.put("id_ingredient", 16);

        long resCocktail15 = db.insert("cocktail", null, vCocktail15);
        long resIngredient37 = db.insert("ingredient", null, vIngredient37);
        long resCompose63 = db.insert("compose", null, vCompose63);
        long resCompose64 = db.insert("compose", null, vCompose64);
        long resCompose65 = db.insert("compose", null, vCompose65);
        long resCompose66 = db.insert("compose", null, vCompose66);

        Log.d("dbInit", "insert cocktail: " + resCocktail15);
        Log.d("dbInit", "insert ingredient: " + resIngredient37);
        Log.d("dbInit", "insert compose: " + resCompose63);
        Log.d("dbInit", "insert compose: " + resCompose64);
        Log.d("dbInit", "insert compose: " + resCompose65);
        Log.d("dbInit", "insert compose: " + resCompose66);

        //----------------------------------------------------------------------------------------
        //                                  GIN FIZZ
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail16 = new ContentValues();
        ContentValues vCompose67 = new ContentValues();
        ContentValues vCompose68 = new ContentValues();
        ContentValues vCompose69 = new ContentValues();
        ContentValues vCompose70 = new ContentValues();

        vCocktail16.put("nom", "Gin Fizz");
        vCocktail16.put("image", "gin_fizz.jpg");
        vCocktail16.put("recette"," Réalisez la recette \"Gin Fizz \" au shaker. \n" +
                "• Frapper le gin, le jus de citron et le sucre avec des glaçons, verser dans le verre tout le contenu du shaker et allonger d'eau gazeuse bien fraîche.\n" +
                "• Servir dans un verre de type \"tumbler\"\n" +
                "• Décor: Une tranche de citron vert sur le bord du verre.\n" +
                "• Contrairement au Tom Collins, le Gin Fizz se sert sans glace.");

        //vIngredient.put("nom", "gin");//36
        //vIngredient.put("nom", "jus de citrons");//21
        //vIngredient.put("nom", "sirop sucre de canne");//10
        //vIngredient.put("nom", "eau gazeuse");//9

        vCompose67.put("id_cocktail", 16);
        vCompose67.put("id_ingredient", 36);
        vCompose68.put("id_cocktail", 16);
        vCompose68.put("id_ingredient", 21);
        vCompose69.put("id_cocktail", 16);
        vCompose69.put("id_ingredient", 10);
        vCompose70.put("id_cocktail", 16);
        vCompose70.put("id_ingredient", 9);

        long resCocktail16 = db.insert("cocktail", null, vCocktail16);
        //long resIngredient15 = db.insert("ingredient", null, vIngredient15);
        long resCompose67 = db.insert("compose", null, vCompose67);
        long resCompose68 = db.insert("compose", null, vCompose68);
        long resCompose69 = db.insert("compose", null, vCompose69);
        long resCompose70 = db.insert("compose", null, vCompose70);

        Log.d("dbInit", "insert cocktail: " + resCocktail16);
        //Log.d("dbInit", "insert ingredient: " + resIngredient15);
        Log.d("dbInit", "insert compose: " + resCompose67);
        Log.d("dbInit", "insert compose: " + resCompose68);
        Log.d("dbInit", "insert compose: " + resCompose69);
        Log.d("dbInit", "insert compose: " + resCompose70);

        //----------------------------------------------------------------------------------------
        //                                  FUN AT THE BEACH
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail17 = new ContentValues();
        ContentValues vIngredient38 = new ContentValues();
        ContentValues vIngredient39 = new ContentValues();
        ContentValues vCompose71 = new ContentValues();
        ContentValues vCompose72 = new ContentValues();
        ContentValues vCompose73 = new ContentValues();
        ContentValues vCompose74 = new ContentValues();
        ContentValues vCompose75 = new ContentValues();

        vCocktail17.put("nom", "Fun at the beach");
        vCocktail17.put("image", "fun_at_the_beach.jpg");
        vCocktail17.put("recette"," • Réalisez la recette \"Fun at the beach \" directement dans le verre. \n" +
                "• Verser sur des glaçons, mélanger et compléter avec les jus de fruits.\n" +
                "• Servir dans un verre de type \"tumbler\"\n" +
                "• Le cocktail souvent imité, jamais égalé. La mûroise peut aisément être remplacée par du Chambord");

        //vIngredient.put("nom", "vodka");//1
        vIngredient38.put("nom", "midori");//38
        vIngredient39.put("nom", "crème de mûre");//39
        //vIngredient.put("nom", "jus d'oranges");//22
        //vIngredient.put("nom", "jus de cranberry");//5

        vCompose71.put("id_cocktail", 17);
        vCompose71.put("id_ingredient", 1);
        vCompose72.put("id_cocktail", 17);
        vCompose72.put("id_ingredient", 38);
        vCompose73.put("id_cocktail", 17);
        vCompose73.put("id_ingredient", 39);
        vCompose74.put("id_cocktail", 17);
        vCompose74.put("id_ingredient", 22);
        vCompose75.put("id_cocktail", 17);
        vCompose75.put("id_ingredient", 5);

        long resCocktail17 = db.insert("cocktail", null, vCocktail17);
        long resIngredient38 = db.insert("ingredient", null, vIngredient38);
        long resIngredient39 = db.insert("ingredient", null, vIngredient39);
        long resCompose71 = db.insert("compose", null, vCompose71);
        long resCompose72 = db.insert("compose", null, vCompose72);
        long resCompose73 = db.insert("compose", null, vCompose73);
        long resCompose74 = db.insert("compose", null, vCompose74);
        long resCompose75 = db.insert("compose", null, vCompose75);

        Log.d("dbInit", "insert cocktail: " + resCocktail17);
        Log.d("dbInit", "insert ingredient: " + resIngredient38);
        Log.d("dbInit", "insert ingredient: " + resIngredient39);
        Log.d("dbInit", "insert compose: " + resCompose71);
        Log.d("dbInit", "insert compose: " + resCompose72);
        Log.d("dbInit", "insert compose: " + resCompose73);
        Log.d("dbInit", "insert compose: " + resCompose74);
        Log.d("dbInit", "insert compose: " + resCompose75);


        //----------------------------------------------------------------------------------------
        //                                  VODKA RED BULL
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail18 = new ContentValues();
        ContentValues vIngredient40 = new ContentValues();
        ContentValues vCompose76 = new ContentValues();
        ContentValues vCompose77 = new ContentValues();

        vCocktail18.put("nom", "Vodka red bull");
        vCocktail18.put("image", "vodka_redbulljpg.jpg");
        vCocktail18.put("recette","Réalisez la recette \"Vodka red bull \" directement dans le verre. \n" +
                "• Verser la vodka dans le verre contenant des glaçons et compléter avec le red bul.\n" +
                "• Servir dans un verre de type \"tumbler\"\n" +
                "• Décor: Décorer de quelques sucreries en forme de nounours ou de deux tranches de citron vert.\n" +
                "• Attention : ce cocktail est bon, mais il est dangereux de mélanger une boisson énergétique avec de l'alcool.");

        //vIngredient.put("nom", "vodka");//1
        vIngredient40.put("nom", "energy drink");//40

        vCompose76.put("id_cocktail", 18);
        vCompose76.put("id_ingredient", 1);
        vCompose77.put("id_cocktail", 18);
        vCompose77.put("id_ingredient", 40);

        long resCocktail18 = db.insert("cocktail", null, vCocktail18);
        long resIngredient40 = db.insert("ingredient", null, vIngredient40);
        long resCompose76 = db.insert("compose", null, vCompose76);
        long resCompose77 = db.insert("compose", null, vCompose77);

        Log.d("dbInit", "insert cocktail: " + resCocktail18);
        Log.d("dbInit", "insert ingredient: " + resIngredient40);
        Log.d("dbInit", "insert compose: " + resCompose76);
        Log.d("dbInit", "insert compose: " + resCompose77);


        //----------------------------------------------------------------------------------------
        //                                  RAISA
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail19 = new ContentValues();
        ContentValues vIngredient41 = new ContentValues();
        ContentValues vCompose78 = new ContentValues();
        ContentValues vCompose79 = new ContentValues();
        ContentValues vCompose80 = new ContentValues();

        vCocktail19.put("nom", "Raisa");
        vCocktail19.put("image", "raisa.jpg");
        vCocktail19.put("recette"," Réalisez la recette \"Raisa \" au shaker. \n" +
                "• Mettez quelques glaçons dans un shaker et ajoutez les ingrédients, agitez vigoureusement pour bien mélanger.\n" +
                "\n" +
                "Remplissez les 2/3 d'un verre à vin ou à whisky de glace pilée et passez le mélange. \n" +
                "\n" +
                "• Servir dans un verre de type \"tumbler\"");

        //vIngredient.put("nom", "vodka");//1
        vIngredient41.put("nom", "liqueur d'abricots");//41
        //vIngredient.put("nom", "jus de citrons verts");//7

        vCompose78.put("id_cocktail", 19);
        vCompose78.put("id_ingredient", 1);
        vCompose79.put("id_cocktail", 19);
        vCompose79.put("id_ingredient", 41);
        vCompose80.put("id_cocktail", 19);
        vCompose80.put("id_ingredient", 7);

        long resCocktail19 = db.insert("cocktail", null, vCocktail19);
        long resIngredient41 = db.insert("ingredient", null, vIngredient41);
        long resCompose78 = db.insert("compose", null, vCompose78);
        long resCompose79 = db.insert("compose", null, vCompose79);
        long resCompose80 = db.insert("compose", null, vCompose80);

        Log.d("dbInit", "insert cocktail: " + resCocktail19);
        Log.d("dbInit", "insert ingredient: " + resIngredient41);
        Log.d("dbInit", "insert compose: " + resCompose78);
        Log.d("dbInit", "insert compose: " + resCompose79);
        Log.d("dbInit", "insert compose: " + resCompose80);


        //----------------------------------------------------------------------------------------
        //                                  BLACK LIME
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail20 = new ContentValues();
        ContentValues vIngredient42 = new ContentValues();
        ContentValues vCompose81 = new ContentValues();
        ContentValues vCompose82 = new ContentValues();
        ContentValues vCompose83 = new ContentValues();

        vCocktail20.put("nom", "Black Lime");
        vCocktail20.put("image", "black_lime.jpg");
        vCocktail20.put("recette"," Réalisez la recette \"Black Lime \" directement dans le verre. \n" +
                "• Ecrasez des petits dés de citron vert avec un trait de sucre de canne et de la glaçe et allongez de vodka Black Eristoff.\n" +
                "• Servir dans un verre de type \"old fashioned\"\n" +
                "• Recete proposée par Black Eristoff.");

        //vIngredient.put("nom", "citrons verts");//18
        //vIngredient.put("nom", "sirop sucre de canne");//10
        vIngredient42.put("nom", "vodka noire");//42

        vCompose81.put("id_cocktail", 20);
        vCompose81.put("id_ingredient", 18);
        vCompose82.put("id_cocktail", 20);
        vCompose82.put("id_ingredient", 10);
        vCompose83.put("id_cocktail", 20);
        vCompose83.put("id_ingredient", 42);

        long resCocktail20 = db.insert("cocktail", null, vCocktail20);
        long resIngredient42 = db.insert("ingredient", null, vIngredient42);
        long resCompose81 = db.insert("compose", null, vCompose81);
        long resCompose82 = db.insert("compose", null, vCompose82);
        long resCompose83 = db.insert("compose", null, vCompose83);

        Log.d("dbInit", "insert cocktail: " + resCocktail20);
        Log.d("dbInit", "insert ingredient: " + resIngredient42);
        Log.d("dbInit", "insert compose: " + resCompose81);
        Log.d("dbInit", "insert compose: " + resCompose82);
        Log.d("dbInit", "insert compose: " + resCompose83);


        //----------------------------------------------------------------------------------------
        //                                  RED LION
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail21 = new ContentValues();
        ContentValues vCompose84 = new ContentValues();
        ContentValues vCompose85 = new ContentValues();
        ContentValues vCompose86 = new ContentValues();
        ContentValues vCompose87 = new ContentValues();

        vCocktail21.put("nom", "Red Lion");
        vCocktail21.put("image", "red_lion.jpg");
        vCocktail21.put("recette","• Réalisez la recette \"Red lion \" au shaker. \n" +
                "• Au shaker. Servir dans un verre à cocktail.\n" +
                "\n" +
                "• Servir dans un verre de type \"verre à martini\"\n" +
                "• Décor: Aucune décoration");

        //vIngredient.put("nom", "triple sec");//15
        //vIngredient.put("nom", "gin");//36
        //vIngredient.put("nom", "jus d'oranges");//22
        //vIngredient.put("nom", "jus de citrons");//21

        vCompose84.put("id_cocktail", 21);
        vCompose84.put("id_ingredient", 15);
        vCompose85.put("id_cocktail", 21);
        vCompose85.put("id_ingredient", 36);
        vCompose86.put("id_cocktail", 21);
        vCompose86.put("id_ingredient", 22);
        vCompose87.put("id_cocktail", 21);
        vCompose87.put("id_ingredient", 21);

        long resCocktail21 = db.insert("cocktail", null, vCocktail21);
        //long resIngredient20 = db.insert("ingredient", null, vIngredient20);
        long resCompose84 = db.insert("compose", null, vCompose84);
        long resCompose85 = db.insert("compose", null, vCompose85);
        long resCompose86 = db.insert("compose", null, vCompose86);
        long resCompose87 = db.insert("compose", null, vCompose87);

        Log.d("dbInit", "insert cocktail: " + resCocktail21);
        //Log.d("dbInit", "insert ingredient: " + resIngredient20);
        Log.d("dbInit", "insert compose: " + resCompose84);
        Log.d("dbInit", "insert compose: " + resCompose85);
        Log.d("dbInit", "insert compose: " + resCompose86);
        Log.d("dbInit", "insert compose: " + resCompose87);


        //----------------------------------------------------------------------------------------
        //                                  SUNBURN
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail22 = new ContentValues();
        ContentValues vCompose88 = new ContentValues();
        ContentValues vCompose89 = new ContentValues();
        ContentValues vCompose90 = new ContentValues();

        vCocktail22.put("nom", "Sunburn");
        vCocktail22.put("image", "sunburn.jpg");
        vCocktail22.put("recette","• Réalisez la recette \"Sunburn \" au shaker. \n" +
                "• Frappez au shaker avec des glaçons, verser en retenant les glaçons.\n" +
                "• Servir dans un verre de type \"verre à martini\"");

        //vIngredient.put("nom", "triple sec");//15
        //vIngredient.put("nom", "tequila");//14
        //vIngredient.put("nom", "jus de cranberry");//5

        vCompose88.put("id_cocktail", 22);
        vCompose88.put("id_ingredient", 15);
        vCompose89.put("id_cocktail", 22);
        vCompose89.put("id_ingredient", 14);
        vCompose90.put("id_cocktail", 22);
        vCompose90.put("id_ingredient", 5);

        long resCocktail22 = db.insert("cocktail", null, vCocktail22);
        //long resIngredient21 = db.insert("ingredient", null, vIngredient21);
        long resCompose88 = db.insert("compose", null, vCompose88);
        long resCompose89 = db.insert("compose", null, vCompose89);
        long resCompose90 = db.insert("compose", null, vCompose90);

        Log.d("dbInit", "insert cocktail: " + resCocktail22);
        //Log.d("dbInit", "insert ingredient: " + resIngredient21);
        Log.d("dbInit", "insert compose: " + resCompose88);
        Log.d("dbInit", "insert compose: " + resCompose89);
        Log.d("dbInit", "insert compose: " + resCompose90);


        //----------------------------------------------------------------------------------------
        //                                  ZERO ABSOLU
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail23 = new ContentValues();
        ContentValues vIngredient43 = new ContentValues();
        ContentValues vCompose91 = new ContentValues();
        ContentValues vCompose92 = new ContentValues();

        vCocktail23.put("nom", "Zero Absolu");
        vCocktail23.put("image", "zero_absolu.jpeg");
        vCocktail23.put("recette","• Réalisez la recette \"Zéro Absolu \" au shaker. \n" +
                "• Frappez les ingrédients au shaker, puis passez dans le verre sur de la glace pilée. Servez rapidement.\n" +
                "• Servir dans un verre de type \"verre à martini\"\n" +
                "• Décor: Aucune décoration\n" +
                "• Une sensation de glacé au moment de la consomation, ce cocktail ne peut que vous réchauffer. A reserver aux amateurs de sensations fortes!");

        //vIngredient.put("nom", "vodka");//1
        vIngredient43.put("nom", "liqueur de menthe verte (get 27)");//43

        vCompose91.put("id_cocktail", 23);
        vCompose91.put("id_ingredient", 1);
        vCompose92.put("id_cocktail", 23);
        vCompose92.put("id_ingredient", 43);

        long resCocktail23 = db.insert("cocktail", null, vCocktail23);
        long resIngredient43 = db.insert("ingredient", null, vIngredient43);
        long resCompose91 = db.insert("compose", null, vCompose91);
        long resCompose92 = db.insert("compose", null, vCompose92);

        Log.d("dbInit", "insert cocktail: " + resCocktail23);
        Log.d("dbInit", "insert ingredient: " + resIngredient43);
        Log.d("dbInit", "insert compose: " + resCompose91);
        Log.d("dbInit", "insert compose: " + resCompose92);


        //----------------------------------------------------------------------------------------
        //                                  HYPERCOOLING
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail24 = new ContentValues();
        ContentValues vIngredient44 = new ContentValues();
        ContentValues vIngredient45 = new ContentValues();
        ContentValues vIngredient46 = new ContentValues();
        ContentValues vIngredient47 = new ContentValues();
        ContentValues vCompose93 = new ContentValues();
        ContentValues vCompose94 = new ContentValues();
        ContentValues vCompose95 = new ContentValues();
        ContentValues vCompose96 = new ContentValues();
        ContentValues vCompose97 = new ContentValues();
        ContentValues vCompose98 = new ContentValues();
        ContentValues vCompose99 = new ContentValues();

        vCocktail24.put("nom", "Hypercooling");
        vCocktail24.put("image", "hypercooling.jpg");
        vCocktail24.put("recette","• Réalisez la recette \"Hypercooling \" au shaker. \n" +
                "• Réalisez des glaçons avec de l'eau et un peu de sirop de menthe glaciale, pour des glaçons colorés et encore plus frais.\n" +
                "\n" +
                "Frappez au shaker la vodka, la liqueur de menthe verte, le sirop de citron avec des glaçons. Versez le tout dans le verre, ajoutez l'eau gazeuse et nappez de chantilly. Faites couler le sirop de menthe sur la chantilly et placez 3 glaçons colorés à la menthe glaciale.\n" +
                "\n" +
                "• Servir dans un verre de type \"verre à martini\"\n" +
                "• Décor: Une feuille de menthe, et les glaçons colorés.\n" +
                "• Utilisez de préférence des sirops \"Giffard\".");

        vIngredient44.put("nom", "eau");//44
        vIngredient45.put("nom", "sirop de menthe");//45
        vIngredient46.put("nom", "crème chantilly");//46
        vIngredient47.put("nom", "limonade");//47
        //vIngredient.put("nom", "jus de citrons");//21
        //vIngredient.put("nom", "liqueur de menthe verte (get 27)");//43
        //vIngredient.put("nom", "vodka");//1

        vCompose93.put("id_cocktail", 24);
        vCompose93.put("id_ingredient", 44);
        vCompose94.put("id_cocktail", 24);
        vCompose94.put("id_ingredient", 45);
        vCompose95.put("id_cocktail", 24);
        vCompose95.put("id_ingredient", 46);
        vCompose96.put("id_cocktail", 24);
        vCompose96.put("id_ingredient", 47);
        vCompose97.put("id_cocktail", 24);
        vCompose97.put("id_ingredient", 21);
        vCompose98.put("id_cocktail", 24);
        vCompose98.put("id_ingredient", 43);
        vCompose99.put("id_cocktail", 24);
        vCompose99.put("id_ingredient", 1);

        long resCocktail24 = db.insert("cocktail", null, vCocktail24);
        long resIngredient44 = db.insert("ingredient", null, vIngredient44);
        long resIngredient45 = db.insert("ingredient", null, vIngredient45);
        long resIngredient46 = db.insert("ingredient", null, vIngredient46);
        long resIngredient47 = db.insert("ingredient", null, vIngredient47);
        long resCompose93 = db.insert("compose", null, vCompose93);
        long resCompose94 = db.insert("compose", null, vCompose94);
        long resCompose95 = db.insert("compose", null, vCompose95);
        long resCompose96 = db.insert("compose", null, vCompose96);
        long resCompose97 = db.insert("compose", null, vCompose97);
        long resCompose98 = db.insert("compose", null, vCompose98);
        long resCompose99 = db.insert("compose", null, vCompose99);

        Log.d("dbInit", "insert cocktail: " + resCocktail24);
        Log.d("dbInit", "insert ingredient: " + resIngredient44);
        Log.d("dbInit", "insert ingredient: " + resIngredient45);
        Log.d("dbInit", "insert ingredient: " + resIngredient46);
        Log.d("dbInit", "insert ingredient: " + resIngredient47);
        Log.d("dbInit", "insert compose: " + resCompose93);
        Log.d("dbInit", "insert compose: " + resCompose94);
        Log.d("dbInit", "insert compose: " + resCompose95);
        Log.d("dbInit", "insert compose: " + resCompose96);
        Log.d("dbInit", "insert compose: " + resCompose97);
        Log.d("dbInit", "insert compose: " + resCompose98);
        Log.d("dbInit", "insert compose: " + resCompose99);


        //----------------------------------------------------------------------------------------
        //                                  HONEYMOON
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail25 = new ContentValues();
        ContentValues vIngredient48 = new ContentValues();
        ContentValues vIngredient49 = new ContentValues();
        ContentValues vCompose100 = new ContentValues();
        ContentValues vCompose101 = new ContentValues();
        ContentValues vCompose102 = new ContentValues();
        ContentValues vCompose103 = new ContentValues();

        vCocktail25.put("nom", "Honeymoon");
        vCocktail25.put("image", "honeymoon.jpg");
        vCocktail25.put("recette"," Réalisez la recette \"Honeymoon \" au shaker. \n" +
                "• Frapper et passer dans le verre. On peut remplacer le sirop d'érable par du miel.\n" +
                "• Servir dans un verre de type \"tumbler\"\n" +
                "• Décor: Décorer avec un zeste d'orange\n" +
                "• A déguster le soir de votre lune de miel");

        //vIngredient.put("nom", "jus d'oranges");//22
        vIngredient48.put("nom", "jus de pommes");//48
        vIngredient49.put("nom", "sirop d'érable");//49
        //vIngredient.put("nom", "jus de citrons verts");//7

        vCompose100.put("id_cocktail", 25);
        vCompose100.put("id_ingredient", 22);
        vCompose101.put("id_cocktail", 25);
        vCompose101.put("id_ingredient", 48);
        vCompose102.put("id_cocktail", 25);
        vCompose102.put("id_ingredient", 49);
        vCompose103.put("id_cocktail", 25);
        vCompose103.put("id_ingredient", 7);

        long resCocktail25 = db.insert("cocktail", null, vCocktail25);
        long resIngredient48 = db.insert("ingredient", null, vIngredient48);
        long resIngredient49 = db.insert("ingredient", null, vIngredient49);
        long resCompose100 = db.insert("compose", null, vCompose100);
        long resCompose101 = db.insert("compose", null, vCompose101);
        long resCompose102 = db.insert("compose", null, vCompose102);
        long resCompose103 = db.insert("compose", null, vCompose103);

        Log.d("dbInit", "insert cocktail: " + resCocktail25);
        Log.d("dbInit", "insert ingredient: " + resIngredient48);
        Log.d("dbInit", "insert ingredient: " + resIngredient49);
        Log.d("dbInit", "insert compose: " + resCompose100);
        Log.d("dbInit", "insert compose: " + resCompose101);
        Log.d("dbInit", "insert compose: " + resCompose102);
        Log.d("dbInit", "insert compose: " + resCompose103);


        //----------------------------------------------------------------------------------------
        //                                  KAMIKAZE BLUE
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail26 = new ContentValues();
        ContentValues vCompose104 = new ContentValues();
        ContentValues vCompose105 = new ContentValues();
        ContentValues vCompose106 = new ContentValues();

        vCocktail26.put("nom", "Kamikaze blue");
        vCocktail26.put("image", "kamikaze_blue.jpg");
        vCocktail26.put("recette","• Réalisez la recette \"Kamikaze blue \" directement dans le verre. \n" +
                "• Servir et boire d'un trait. \n" +
                "• Servir dans un verre de type \"shooter\"");

        //vIngredient.put("nom", "vodka");//1
        //vIngredient.put("nom", "curaçao bleu");//20
        //vIngredient.put("nom", "jus de citrons");//21

        vCompose104.put("id_cocktail", 26);
        vCompose104.put("id_ingredient", 1);
        vCompose105.put("id_cocktail", 26);
        vCompose105.put("id_ingredient", 20);
        vCompose106.put("id_cocktail", 26);
        vCompose106.put("id_ingredient", 21);

        long resCocktail26 = db.insert("cocktail", null, vCocktail26);
        //long resIngredient25 = db.insert("ingredient", null, vIngredient25);
        long resCompose104 = db.insert("compose", null, vCompose104);
        long resCompose105 = db.insert("compose", null, vCompose105);
        long resCompose106 = db.insert("compose", null, vCompose106);

        Log.d("dbInit", "insert cocktail: " + resCocktail26);
        //Log.d("dbInit", "insert ingredient: " + resIngredient25);
        Log.d("dbInit", "insert compose: " + resCompose104);
        Log.d("dbInit", "insert compose: " + resCompose105);
        Log.d("dbInit", "insert compose: " + resCompose106);


        //----------------------------------------------------------------------------------------
        //                                  SILVER FIZZ
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail27 = new ContentValues();
        ContentValues vIngredient50 = new ContentValues();
        ContentValues vCompose107 = new ContentValues();
        ContentValues vCompose108 = new ContentValues();
        ContentValues vCompose109 = new ContentValues();
        ContentValues vCompose110 = new ContentValues();
        ContentValues vCompose111 = new ContentValues();

        vCocktail27.put("nom", "Silver Fizz");
        vCocktail27.put("image", "silver_fizz.jpg");
        vCocktail27.put("recette","• Réalisez la recette \"Silver Fizz \" au shaker. \n" +
                "• Frapper les ingrédients et passer dans le verre rempli de glaçons. Compléter d'eau gazeuse et servir.\n" +
                "• Servir dans un verre de type \"tumbler\"\n" +
                "• Décor: Décorer avec une tranche de citron\n" +
                "• Un gin fizz avec un blanc d'oeuf pour obtenir une couleur argentée.");

        //vIngredient.put("nom", "gin");//36
        //vIngredient.put("nom", "jus de citrons");//21
        //vIngredient.put("nom", "sirop sucre de canne");//10
        vIngredient50.put("nom", "oeufs");//50
        //vIngredient.put("nom", "eau gazeuse");//9

        vCompose107.put("id_cocktail", 27);
        vCompose107.put("id_ingredient", 36);
        vCompose108.put("id_cocktail", 27);
        vCompose108.put("id_ingredient", 21);
        vCompose109.put("id_cocktail", 27);
        vCompose109.put("id_ingredient", 10);
        vCompose110.put("id_cocktail", 27);
        vCompose110.put("id_ingredient", 50);
        vCompose111.put("id_cocktail", 27);
        vCompose111.put("id_ingredient", 9);

        long resCocktail27 = db.insert("cocktail", null, vCocktail27);
        long resIngredient50 = db.insert("ingredient", null, vIngredient50);
        long resCompose107 = db.insert("compose", null, vCompose107);
        long resCompose108 = db.insert("compose", null, vCompose108);
        long resCompose109 = db.insert("compose", null, vCompose109);
        long resCompose110 = db.insert("compose", null, vCompose110);
        long resCompose111 = db.insert("compose", null, vCompose111);

        Log.d("dbInit", "insert cocktail: " + resCocktail27);
        Log.d("dbInit", "insert ingredient: " + resIngredient50);
        Log.d("dbInit", "insert compose: " + resCompose107);
        Log.d("dbInit", "insert compose: " + resCompose108);
        Log.d("dbInit", "insert compose: " + resCompose109);
        Log.d("dbInit", "insert compose: " + resCompose110);
        Log.d("dbInit", "insert compose: " + resCompose111);


        //----------------------------------------------------------------------------------------
        //                                  VODKA SALTY DOG
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail28 = new ContentValues();
        ContentValues vCompose112 = new ContentValues();
        ContentValues vCompose113 = new ContentValues();

        vCocktail28.put("nom", "Vodka salty dog");
        vCocktail28.put("image", "vodka_salty_dog.png");
        vCocktail28.put("recette","• Réalisez la recette \"Vodka salty dog \" au shaker. \n" +
                "• Frapper les ingrédients avec des glaçons dans le shaker. Verser dans les verres à shot. Boire d'un trait.\n" +
                "• Servir dans un verre de type \"shooter\"\n" +
                "• Décor: Aucune décoration");

        //vIngredient.put("nom", "jus de pamplemousse");//24
        //vIngredient.put("nom", "vodka");//1

        vCompose112.put("id_cocktail", 28);
        vCompose112.put("id_ingredient", 24);
        vCompose113.put("id_cocktail", 28);
        vCompose113.put("id_ingredient", 1);

        long resCocktail28 = db.insert("cocktail", null, vCocktail28);
        //long resIngredient27 = db.insert("ingredient", null, vIngredient27);
        long resCompose112 = db.insert("compose", null, vCompose112);
        long resCompose113 = db.insert("compose", null, vCompose113);

        Log.d("dbInit", "insert cocktail: " + resCocktail28);
        //Log.d("dbInit", "insert ingredient: " + resIngredient27);
        Log.d("dbInit", "insert compose: " + resCompose112);
        Log.d("dbInit", "insert compose: " + resCompose113);


        //----------------------------------------------------------------------------------------
        //                                  STINGER
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail29 = new ContentValues();
        ContentValues vIngredient51 = new ContentValues();
        ContentValues vIngredient52 = new ContentValues();
        ContentValues vCompose114 = new ContentValues();
        ContentValues vCompose115 = new ContentValues();

        vCocktail29.put("nom", "Stinger");
        vCocktail29.put("image", "stinger.jpg");
        vCocktail29.put("recette","• Réalisez la recette \"Stinger \" directement dans le verre. \n" +
                "• Placez quelques glaçons dans le verre, versez les ingrédients et mélangez avec une cuillère.\n" +
                "• Servir dans un verre de type \"old fashioned\"\n" +
                "• Décor: Une brindille de menthe.\n" +
                "• Appelé parfois à tort Stringer.");

        vIngredient51.put("nom", "cognac");//51
        vIngredient52.put("nom", "liqueur de menthe blanche (get 31)");//52

        vCompose114.put("id_cocktail", 29);
        vCompose114.put("id_ingredient", 51);
        vCompose115.put("id_cocktail", 29);
        vCompose115.put("id_ingredient", 52);

        long resCocktail29 = db.insert("cocktail", null, vCocktail29);
        long resIngredient51 = db.insert("ingredient", null, vIngredient51);
        long resIngredient52 = db.insert("ingredient", null, vIngredient52);
        long resCompose114 = db.insert("compose", null, vCompose114);
        long resCompose115 = db.insert("compose", null, vCompose115);

        Log.d("dbInit", "insert cocktail: " + resCocktail29);
        Log.d("dbInit", "insert ingredient: " + resIngredient51);
        Log.d("dbInit", "insert ingredient: " + resIngredient52);
        Log.d("dbInit", "insert compose: " + resCompose114);
        Log.d("dbInit", "insert compose: " + resCompose115);


        //----------------------------------------------------------------------------------------
        //                                  APPLE JACK
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail30 = new ContentValues();
        ContentValues vIngredient53 = new ContentValues();
        ContentValues vCompose116 = new ContentValues();
        ContentValues vCompose117 = new ContentValues();
        ContentValues vCompose118 = new ContentValues();

        vCocktail30.put("nom", "Apple jack");
        vCocktail30.put("image", "apple_jack.jpg");
        vCocktail30.put("recette","• Réalisez la recette \"Apple jack (Applejack) \" au shaker. \n" +
                "• Au shaker. Servir dans un verre à cocktail.\n" +
                "\n" +
                "• Servir dans un verre de type \"verre à martini\"\n" +
                "• Décor: Aucune décoration\n" +
                "• Certains y ajoutent 2 cl d'alcool blanc de poire...");

        //vIngredient.put("nom", "jus de citrons");//21
        vIngredient53.put("nom", "calvados");//53
        //vIngredient.put("nom", "sirop de grenadine");//23

        vCompose116.put("id_cocktail", 30);
        vCompose116.put("id_ingredient", 21);
        vCompose117.put("id_cocktail", 30);
        vCompose117.put("id_ingredient", 53);
        vCompose118.put("id_cocktail", 30);
        vCompose118.put("id_ingredient", 23);

        long resCocktail30 = db.insert("cocktail", null, vCocktail30);
        long resIngredient53 = db.insert("ingredient", null, vIngredient53);
        long resCompose116 = db.insert("compose", null, vCompose116);
        long resCompose117 = db.insert("compose", null, vCompose117);
        long resCompose118 = db.insert("compose", null, vCompose118);

        Log.d("dbInit", "insert cocktail: " + resCocktail30);
        Log.d("dbInit", "insert ingredient: " + resIngredient53);
        Log.d("dbInit", "insert compose: " + resCompose116);
        Log.d("dbInit", "insert compose: " + resCompose117);
        Log.d("dbInit", "insert compose: " + resCompose118);

        //----------------------------------------------------------------------------------------
        //                                  RUSSIAN ISLAND
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail31 = new ContentValues();
        //ContentValues vIngredient30 = new ContentValues();
        ContentValues vCompose119 = new ContentValues();
        ContentValues vCompose120 = new ContentValues();
        ContentValues vCompose121 = new ContentValues();
        ContentValues vCompose122 = new ContentValues();
        ContentValues vCompose123 = new ContentValues();
        ContentValues vCompose124 = new ContentValues();
        ContentValues vCompose125 = new ContentValues();
        ContentValues vCompose126 = new ContentValues();

        vCocktail31.put("nom", "Russian island");
        vCocktail31.put("image", "russian_island.jpg");
        vCocktail31.put("recette","• Réalisez la recette \"Russian Island \" au shaker. \n" +
                "• Préparez la recette du Cocktail Russian Island au shaker. Frapper les ingrédients et servir dans un verre rempli de glace pilée. Puis ajouter le sirop de grenadine pour la décoration.\n" +
                "• Servir dans un verre de type \"tumbler\"");

        //vIngredient.put("nom", "vodka");//1
        //vIngredient30.put("nom", "triple sec");//15
        //vIngredient.put("nom", "jus d'ananas");//4
        //vIngredient.put("nom", "jus de cranberry");//5
        //vIngredient.put("nom", "jus d'orange");//22
        //vIngredient.put("nom", "lait de coco");//13
        //vIngredient.put("nom", "jus de citron");//7
        //vIngredient30.put("nom", "sirop de grenadine");//23

        vCompose119.put("id_cocktail", 31);
        vCompose119.put("id_ingredient", 1);
        vCompose120.put("id_cocktail", 31);
        vCompose120.put("id_ingredient", 15);
        vCompose121.put("id_cocktail", 31);
        vCompose121.put("id_ingredient", 4);
        vCompose122.put("id_cocktail", 31);
        vCompose122.put("id_ingredient", 5);
        vCompose123.put("id_cocktail", 31);
        vCompose123.put("id_ingredient", 22);
        vCompose124.put("id_cocktail", 31);
        vCompose124.put("id_ingredient", 13);
        vCompose125.put("id_cocktail", 31);
        vCompose125.put("id_ingredient", 7);
        vCompose126.put("id_cocktail", 31);
        vCompose126.put("id_ingredient", 23);

        long resCocktail31 = db.insert("cocktail", null, vCocktail31);
        //long resIngredient30 = db.insert("ingredient", null, vIngredient30);
        long resCompose119 = db.insert("compose", null, vCompose119);
        long resCompose120 = db.insert("compose", null, vCompose120);
        long resCompose121 = db.insert("compose", null, vCompose121);
        long resCompose122 = db.insert("compose", null, vCompose122);
        long resCompose123 = db.insert("compose", null, vCompose123);
        long resCompose124 = db.insert("compose", null, vCompose124);
        long resCompose125 = db.insert("compose", null, vCompose125);
        long resCompose126 = db.insert("compose", null, vCompose126);

        Log.d("dbInit", "insert cocktail: " + resCocktail31);
        //Log.d("dbInit", "insert ingredient: " + resIngredient30);
        Log.d("dbInit", "insert compose: " + resCompose119);
        Log.d("dbInit", "insert compose: " + resCompose120);
        Log.d("dbInit", "insert compose: " + resCompose121);
        Log.d("dbInit", "insert compose: " + resCompose122);
        Log.d("dbInit", "insert compose: " + resCompose123);
        Log.d("dbInit", "insert compose: " + resCompose124);
        Log.d("dbInit", "insert compose: " + resCompose125);
        Log.d("dbInit", "insert compose: " + resCompose126);


        //----------------------------------------------------------------------------------------
        //                                  HARVEY WALLBANGER
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail32 = new ContentValues();
        ContentValues vIngredient54 = new ContentValues();
        ContentValues vCompose127 = new ContentValues();
        ContentValues vCompose128 = new ContentValues();
        ContentValues vCompose129 = new ContentValues();

        vCocktail32.put("nom", "Harvey Wallbanger");
        vCocktail32.put("image", "harvey_wallbanger.jpg");
        vCocktail32.put("recette","• Réalisez la recette \"Harvey Wallbanger \" directement dans le verre. \n" +
                "• Verser dans le verre rempli de glaçons la vodka et le jus. Napper ensuite de galliano et servir.\n" +
                "• Servir dans un verre de type \"tumbler\"\n" +
                "• Décor: Une tranche d'orange et une cerise confite piquées avec un cure dents.\n" +
                "• Un screwdriver à l'italienne.");

        //vIngredient.put("nom", "vodka");//1
        vIngredient54.put("nom", "liqueur galliano");//54
        //vIngredient.put("nom", "jus d'orange");//22

        vCompose127.put("id_cocktail", 32);
        vCompose127.put("id_ingredient", 1);
        vCompose128.put("id_cocktail", 32);
        vCompose128.put("id_ingredient", 54);
        vCompose129.put("id_cocktail", 32);
        vCompose129.put("id_ingredient", 22);


        long resCocktail32 = db.insert("cocktail", null, vCocktail32);
        long resIngredient54 = db.insert("ingredient", null, vIngredient54);
        long resCompose127 = db.insert("compose", null, vCompose127);
        long resCompose128 = db.insert("compose", null, vCompose128);
        long resCompose129 = db.insert("compose", null, vCompose129);

        Log.d("dbInit", "insert cocktail: " + resCocktail32);
        Log.d("dbInit", "insert ingredient: " + resIngredient54);
        Log.d("dbInit", "insert compose: " + resCompose127);
        Log.d("dbInit", "insert compose: " + resCompose128);
        Log.d("dbInit", "insert compose: " + resCompose129);

        //----------------------------------------------------------------------------------------
        //                                  MIAMI VICE
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail33 = new ContentValues();
        ContentValues vIngredient55 = new ContentValues();
        ContentValues vIngredient56 = new ContentValues();
        ContentValues vCompose130 = new ContentValues();
        ContentValues vCompose131 = new ContentValues();
        ContentValues vCompose132 = new ContentValues();
        ContentValues vCompose133 = new ContentValues();

        vCocktail33.put("nom", "Harvey Wallbanger");
        vCocktail33.put("image", "miami_vice.jpeg");
        vCocktail33.put("recette","• Réalisez la recette \"Harvey Wallbanger \" directement dans le verre. \n" +
                "• Verser dans le verre rempli de glaçons la vodka et le jus. Napper ensuite de galliano et servir.\n" +
                "• Servir dans un verre de type \"tumbler\"\n" +
                "• Décor: Une tranche d'orange et une cerise confite piquées avec un cure dents.\n" +
                "• Un screwdriver à l'italienne.");

        //vIngredient1.put("nom", "rhum cubain");//6
        vIngredient55.put("nom", "sirop pina colada");//55
        //vIngredient1.put("nom", "jus de citrons verts");//7
        vIngredient56.put("nom", "sirop de fraises");//56

        vCompose130.put("id_cocktail", 33);
        vCompose130.put("id_ingredient", 6);
        vCompose131.put("id_cocktail", 33);
        vCompose131.put("id_ingredient", 55);
        vCompose132.put("id_cocktail", 33);
        vCompose132.put("id_ingredient", 7);
        vCompose133.put("id_cocktail", 33);
        vCompose133.put("id_ingredient", 56);


        long resCocktail33 = db.insert("cocktail", null, vCocktail33);
        long resIngredient55 = db.insert("ingredient", null, vIngredient55);
        long resIngredient56 = db.insert("ingredient", null, vIngredient56);
        long resCompose130 = db.insert("compose", null, vCompose130);
        long resCompose131 = db.insert("compose", null, vCompose131);
        long resCompose132 = db.insert("compose", null, vCompose132);
        long resCompose133 = db.insert("compose", null, vCompose133);

        Log.d("dbInit", "insert cocktail: " + resCocktail33);
        Log.d("dbInit", "insert ingredient: " + resIngredient55);
        Log.d("dbInit", "insert ingredient: " + resIngredient56);
        Log.d("dbInit", "insert compose: " + resCompose130);
        Log.d("dbInit", "insert compose: " + resCompose131);
        Log.d("dbInit", "insert compose: " + resCompose132);
        Log.d("dbInit", "insert compose: " + resCompose133);

        //----------------------------------------------------------------------------------------
        //                                  SCREWDRIVER
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail34 = new ContentValues();
        //ContentValues vIngredient33 = new ContentValues();
        ContentValues vCompose134 = new ContentValues();
        ContentValues vCompose135 = new ContentValues();

        vCocktail34.put("nom", "Screwdriver");
        vCocktail34.put("image", "screwdriver.png");
        vCocktail34.put("recette","• Réalisez la recette \"Screwdriver (Vodka orange) \" au shaker. \n" +
                "• Versez les ingrédients dans le shaker. Frappez, puis servez.\n" +
                "• Servir dans un verre de type \"tumbler\"\n" +
                "• Décor: Décorer avec une tranche d'orange\n" +
                "• Ce cocktail nommé \"Tournevis\" (traduction de screwdriver) et plus connu en France sous le nom de \"Vodka-Orange\".");

        //vIngredient.put("nom", "vodka");//1
        //vIngredient.put("nom", "jus d'orange");//22

        vCompose134.put("id_cocktail", 34);
        vCompose134.put("id_ingredient", 1);
        vCompose135.put("id_cocktail", 34);
        vCompose135.put("id_ingredient", 54);


        long resCocktail34 = db.insert("cocktail", null, vCocktail34);
        //long resIngredient33 = db.insert("ingredient", null, vIngredient33);
        long resCompose134 = db.insert("compose", null, vCompose134);
        long resCompose135 = db.insert("compose", null, vCompose135);

        Log.d("dbInit", "insert cocktail: " + resCocktail34);
        //Log.d("dbInit", "insert ingredient: " + resIngredient33);
        Log.d("dbInit", "insert compose: " + resCompose134);
        Log.d("dbInit", "insert compose: " + resCompose135);


        //----------------------------------------------------------------------------------------
        //                                  EL DIABLO
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail35 = new ContentValues();
        ContentValues vIngredient57 = new ContentValues();
        ContentValues vIngredient58 = new ContentValues();
        ContentValues vCompose136 = new ContentValues();
        ContentValues vCompose137 = new ContentValues();

        vCocktail35.put("nom", "El Diablo");
        vCocktail35.put("image", "el_diablo.png");
        vCocktail35.put("recette","• Réalisez la recette \"El Diablo \" directement dans le verre. \n" +
                "• Ecraser les quartiers de citron à l'aide d'un pilon, remplissez le verre aux deux tiers de glaçons, verser la crème de cassis, la tequila et le ginger ale. Remuez et servir.\n" +
                "• Servir dans un verre de type \"tumbler\"\n" +
                "• Une combinaison diabolique au gingembre.");

        //vIngredient.put("nom", "tequila");//14
        //vIngredient5.put("nom", "citrons verts");//18
        vIngredient57.put("nom", "crème de cassis");//57
        vIngredient58.put("nom", "ginger ale (canada dry)");//58

        vCompose136.put("id_cocktail", 35);
        vCompose136.put("id_ingredient", 1);
        vCompose137.put("id_cocktail", 35);
        vCompose137.put("id_ingredient", 54);


        long resCocktail35 = db.insert("cocktail", null, vCocktail35);
        long resIngredient57 = db.insert("ingredient", null, vIngredient57);
        long resIngredient58 = db.insert("ingredient", null, vIngredient58);
        long resCompose136 = db.insert("compose", null, vCompose136);
        long resCompose137 = db.insert("compose", null, vCompose137);

        Log.d("dbInit", "insert cocktail: " + resCocktail35);
        Log.d("dbInit", "insert ingredient: " + resIngredient57);
        Log.d("dbInit", "insert ingredient: " + resIngredient58);
        Log.d("dbInit", "insert compose: " + resCompose136);
        Log.d("dbInit", "insert compose: " + resCompose137);


        //----------------------------------------------------------------------------------------
        //                                  SIDE CAR
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail36 = new ContentValues();
        //ContentValues vIngredient35 = new ContentValues();
        ContentValues vCompose138 = new ContentValues();
        ContentValues vCompose139 = new ContentValues();

        vCocktail36.put("nom", "Side Car");
        vCocktail36.put("image", "side_car.jpg");
        vCocktail36.put("recette","• Réalisez la recette \"Side-car \" au shaker. \n" +
                "• Frapper les ingrédients dans le shaker avec 5 ou 6 glaçons et servir dans le verre rafraîchi en retenant les glaçons.\n" +
                "• Servir dans un verre de type \"verre à martini\"\n" +
                "• Utilisez de préférence du cointreau.");

        //vIngredient3.put("nom", "triple sec");//15
        //vIngredient28.put("nom", "cognac");//51
        //vIngredient7.put("nom", "jus de citrons");//21

        vCompose138.put("id_cocktail", 36);
        vCompose138.put("id_ingredient", 1);
        vCompose139.put("id_cocktail", 36);
        vCompose139.put("id_ingredient", 54);


        long resCocktail36 = db.insert("cocktail", null, vCocktail36);
        //long resIngredient35 = db.insert("ingredient", null, vIngredient35);
        long resCompose138 = db.insert("compose", null, vCompose138);
        long resCompose139 = db.insert("compose", null, vCompose139);

        Log.d("dbInit", "insert cocktail: " + resCocktail36);
        //Log.d("dbInit", "insert ingredient: " + resIngredient35);
        Log.d("dbInit", "insert compose: " + resCompose138);
        Log.d("dbInit", "insert compose: " + resCompose139);


        //----------------------------------------------------------------------------------------
        //                                  PUSSYFOOT JONHSON
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail37 = new ContentValues();
        //ContentValues vIngredient36 = new ContentValues();
        ContentValues vCompose140 = new ContentValues();
        ContentValues vCompose141 = new ContentValues();
        ContentValues vCompose142 = new ContentValues();
        ContentValues vCompose143 = new ContentValues();

        vCocktail37.put("nom", "Pussyfoot jonhson");
        vCocktail37.put("image", "pussyfoot.jpg");
        vCocktail37.put("recette","• Réalisez la recette \"Pussyfoot jonhson \" au shaker. \n" +
                "• Frapper les ingrédients très fort et servir.\n" +
                "• Servir dans un verre de type \"tumbler\"\n" +
                "• Décor: Décorer avec le trait de sirop de grenadine.\n" +
                "• Un cocktail qui a beaucoup de variantes à son actif. Mais celle-ci est l'originale !");

        //vIngredient26.put("nom", "oeufs");//50
        //vIngredient8.put("nom", "sirop de grenadine");//23
        //vIngredient8.put("nom", "jus d'oranges");//22
        //vIngredient7.put("nom", "jus de citrons");//21

        vCompose140.put("id_cocktail", 37);
        vCompose140.put("id_ingredient", 50);
        vCompose141.put("id_cocktail", 37);
        vCompose141.put("id_ingredient", 23);
        vCompose142.put("id_cocktail", 37);
        vCompose142.put("id_ingredient", 22);
        vCompose143.put("id_cocktail", 37);
        vCompose143.put("id_ingredient", 21);


        long resCocktail37 = db.insert("cocktail", null, vCocktail37);
        //long resIngredient36 = db.insert("ingredient", null, vIngredient36);
        long resCompose140 = db.insert("compose", null, vCompose140);
        long resCompose141 = db.insert("compose", null, vCompose141);
        long resCompose142 = db.insert("compose", null, vCompose142);
        long resCompose143 = db.insert("compose", null, vCompose143);

        Log.d("dbInit", "insert cocktail: " + resCocktail37);
        //Log.d("dbInit", "insert ingredient: " + resIngredient36);
        Log.d("dbInit", "insert compose: " + resCompose140);
        Log.d("dbInit", "insert compose: " + resCompose141);
        Log.d("dbInit", "insert compose: " + resCompose142);
        Log.d("dbInit", "insert compose: " + resCompose143);


        //----------------------------------------------------------------------------------------
        //                                  JACK'S SPECIAL
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail38 = new ContentValues();
        ContentValues vIngredient59 = new ContentValues();
        ContentValues vCompose144 = new ContentValues();
        ContentValues vCompose145 = new ContentValues();
        ContentValues vCompose146 = new ContentValues();

        vCocktail38.put("nom", "Jack's Special");
        vCocktail38.put("image", "jack_s_special.png");
        vCocktail38.put("recette","• Réalisez la recette \"Jack's Special \" au mixer. \n" +
                "• Mixer avec de la glace pilée. Verser et décorer avec deux fraises.\n" +
                "• Servir dans un verre de type \"tumbler\"\n" +
                "• Décor: fraise fraîche\n" +
                "• Un jus de fraise citroné.");

        vIngredient59.put("nom", "fraises");//59
        //vIngredient.put("nom", "jus d'ananas");//4
        //vIngredient7.put("nom", "jus de citrons");//21

        vCompose144.put("id_cocktail", 38);
        vCompose144.put("id_ingredient", 59);
        vCompose145.put("id_cocktail", 38);
        vCompose145.put("id_ingredient", 4);
        vCompose146.put("id_cocktail", 38);
        vCompose146.put("id_ingredient", 21);


        long resCocktail38 = db.insert("cocktail", null, vCocktail38);
        long resIngredient59 = db.insert("ingredient", null, vIngredient59);
        long resCompose144 = db.insert("compose", null, vCompose144);
        long resCompose145 = db.insert("compose", null, vCompose145);
        long resCompose146 = db.insert("compose", null, vCompose146);

        Log.d("dbInit", "insert cocktail: " + resCocktail38);
        Log.d("dbInit", "insert ingredient: " + resIngredient59);
        Log.d("dbInit", "insert compose: " + resCompose144);
        Log.d("dbInit", "insert compose: " + resCompose145);
        Log.d("dbInit", "insert compose: " + resCompose146);

        //----------------------------------------------------------------------------------------
        //                                  GOLDEN DREAM
        //----------------------------------------------------------------------------------------
        ContentValues vCocktail39 = new ContentValues();
        ContentValues vIngredient60 = new ContentValues();
        ContentValues vCompose147 = new ContentValues();
        ContentValues vCompose148 = new ContentValues();
        ContentValues vCompose149 = new ContentValues();
        ContentValues vCompose150 = new ContentValues();

        vCocktail39.put("nom", "Golden Dream");
        vCocktail39.put("image", "golden_dream.jpg");
        vCocktail39.put("recette","• Réalisez la recette \"Golden Dream \" au shaker. \n" +
                "• Frapper au shaker avec des glaçons et verser dans le verre en filtrant les glaçons.\n" +
                "• Servir dans un verre de type \"verre à martini\"\n" +
                "• Décor: Décorer d'une cerise confite.");

        //vIngredient31.put("nom", "liqueur galliano");//54
        //vIngredient3.put("nom", "triple sec");//15
        //vIngredient8.put("nom", "jus d'oranges");//22
        vIngredient60.put("nom", "crème fraîche liquide");//60

        vCompose147.put("id_cocktail", 39);
        vCompose147.put("id_ingredient", 54);
        vCompose148.put("id_cocktail", 39);
        vCompose148.put("id_ingredient", 15);
        vCompose149.put("id_cocktail", 39);
        vCompose149.put("id_ingredient", 22);
        vCompose150.put("id_cocktail", 39);
        vCompose150.put("id_ingredient", 60);


        long resCocktail39 = db.insert("cocktail", null, vCocktail39);
        long resIngredient60 = db.insert("ingredient", null, vIngredient60);
        long resCompose147 = db.insert("compose", null, vCompose147);
        long resCompose148 = db.insert("compose", null, vCompose148);
        long resCompose149 = db.insert("compose", null, vCompose149);
        long resCompose150 = db.insert("compose", null, vCompose150);

        Log.d("dbInit", "insert cocktail: " + resCocktail39);
        Log.d("dbInit", "insert ingredient: " + resIngredient60);
        Log.d("dbInit", "insert compose: " + resCompose147);
        Log.d("dbInit", "insert compose: " + resCompose148);
        Log.d("dbInit", "insert compose: " + resCompose149);
        Log.d("dbInit", "insert compose: " + resCompose150);




        Log.d("dbInit", "initialized");
    }

    public static ArrayList<String> selectCocktail(Activity activity, String id){
        SQLiteDatabase db = activity.openOrCreateDatabase(DBNAME,MainActivity.MODE_PRIVATE, null);

        String sql = "SELECT nom,recette,image FROM cocktail where id = ?";
        Cursor c = db.rawQuery(sql, new String[]{id});
        ArrayList<String> cocktail = new ArrayList<>();
        if (c.moveToFirst()){
            do {
                cocktail.add(c.getString(0));
                cocktail.add(c.getString(1));
                cocktail.add(c.getString(2));
            } while(c.moveToNext());
        }
        c.close();

        db.close();
        return cocktail;
    }

    public static ArrayList<String> selectIngredients(Activity activity, String id){
        SQLiteDatabase db = activity.openOrCreateDatabase(DBNAME,MainActivity.MODE_PRIVATE, null);

        String sql = "SELECT nom FROM compose,ingredient where compose.id_ingredient = ingredient.id and compose.id_cocktail = ?";
        Cursor c = db.rawQuery(sql, new String[]{id});
        ArrayList<String> ingredients = new ArrayList<>();
        if (c.moveToFirst()){
            do {
                ingredients.add(c.getString(0));
            } while(c.moveToNext());
        }
        c.close();

        db.close();
        return ingredients;
    }

    public static ArrayList<ArrayList<String>> AllCocktail(Activity activity){
        SQLiteDatabase db = activity.openOrCreateDatabase(DBNAME,MainActivity.MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT id, nom, image FROM cocktail", null);
        ArrayList<ArrayList<String>> cocktail= new ArrayList<ArrayList<String>>();
        //ArrayList<Pair<String, String>> cocktail = new ArrayList<Pair<String, String>>();

        if (c.moveToFirst()){
            do {
                ArrayList<String>element= new ArrayList<String>();
                element.add(c.getString(0));
                element.add(c.getString(1));
                element.add(c.getString(2));
                Log.wtf("",c.getString(0));
                cocktail.add(element);

            } while(c.moveToNext());
        }
        c.close();

        db.close();
        return cocktail;
    }

    public static ArrayList<String> findcocktailwIngredients(Activity activity, ArrayList<Integer> id)  //-----Requete BDD laurent -----
    {
        SQLiteDatabase db = activity.openOrCreateDatabase(DBNAME,MainActivity.MODE_PRIVATE, null);
        String request="";
        if (id.size() > 0) {
            request = String.valueOf(id.get(0));
            for (int ia = 1; ia < id.size(); ia++) {
                request += " OR id_ingredient=" + id.get(ia);
            }
        }
        //Cursor c = db.rawQuery("SELECT id_cocktail FROM compose WHERE id_ingredient = 14 OR id_ingredient = 3 OR id_ingredient = 23" , null );
        Cursor c = db.rawQuery("SELECT id_cocktail FROM compose WHERE id_ingredient = " + request  ,null );
        ArrayList<String> listecocktail = new ArrayList<>();
        ArrayList<String> Result = new ArrayList<>();
        if (c.moveToFirst()){
            int occurences;
            do {
                listecocktail.add(c.getString(0));
                occurences = Collections.frequency(listecocktail, c.getString(0));
                if (occurences >= id.size() )
                {
                    Result.add(c.getString(0));
                }
            } while(c.moveToNext());
        }
        c.close();

        db.close();

        return Result;
    }
    public static String findcocktailwIngredients2(Activity activity, ArrayList<Integer> id)  //-----Requete BDD laurent -----
    {
        SQLiteDatabase db = activity.openOrCreateDatabase(DBNAME,MainActivity.MODE_PRIVATE, null);
        String request="";
        if (id.size() > 0) {
            request = String.valueOf(id.get(0));
            for (int ia = 1; ia < id.size(); ia++) {
                request += " OR id_ingredient=" + id.get(ia);
            }
        }
        String requete = "SELECT id_cocktail FROM compose WHERE id_ingredient = " + request;

        db.close();

        return request;
    }

    public static ArrayList<ArrayList<String>> RechercheCocktail(Activity activity, String req){
        SQLiteDatabase db = activity.openOrCreateDatabase(DBNAME,MainActivity.MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT id, nom, image FROM cocktail WHERE id in(SELECT id_cocktail FROM compose WHERE id_ingredient = " + req+")",null);
        ArrayList<ArrayList<String>> cocktail= new ArrayList<ArrayList<String>>();

        //(SELECT id_cocktail FROM compose WHERE id_ingredient =1 )
        //ArrayList<Pair<String, String>> cocktail = new ArrayList<Pair<String, String>>();

        if (c.moveToFirst()){
            do {
                ArrayList<String>element= new ArrayList<String>();
                element.add(c.getString(0));
                element.add(c.getString(1));
                element.add(c.getString(2));
                //Log.wtf("tessst",c.getString(0));
                cocktail.add(element);

            } while(c.moveToNext());
        }
        c.close();

        db.close();
        return cocktail;
    }
}
